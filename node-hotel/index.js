var architect = require("architect");

var appConfig = require('./hotel/Agoda/config').appConfig;
var tree = architect.resolveConfig(appConfig, __dirname);
architect.createApp(tree, function (err, app) {
  if (err) throw err;
  console.log("app ready");
});
