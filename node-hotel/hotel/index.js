module.exports = function setup(options, imports, register) {
    var async = require('async');
    var _ = require('lodash');  
    var Country = require('../hotel/models/country');
    var City = require('../hotel/models/city');
    var Hotel = require('../hotel/models/hotel');
    var Booking = require('../hotel/models/hotelbooking');
    var agodaAvailability = require('./Agoda/availability');
    var agodaPay = require('./Agoda/pay');
    var mongoConfig = require('./Agoda/config').mongoConfig;
    var redisConfig = require('./Agoda/config').redisConfig;
    var mongoose = require('mongoose');
    mongoose.connect(mongoConfig.url());
    mongoose.connection.on('open', function (ref) {
        console.log('Connected to mongo server.');
    });
    mongoose.connection.on('error', function(err){
			console.log('can not connect to mongo : ' + err);
		});
    var redis = require("redis");
    var cacheTTL = 30 * 60; //seconds
    var cache = redis.createClient(redisConfig.port, redisConfig.host);
    cache.auth(redisConfig.auth);
    cache.on("connect", function(){console.log('Redis Connected...')});
    cache.on("error", function (err) {
        console.log("error event - " + cache.host + ":" + cache.port + " - " + err);
    });

    var logger = {
        info: function(i){
            console.info(i);
        },
        warn: function(w){
            console.warn(w);  
        },
        error: function(e){
            console.error(e);
        }
    };
    
    var CityConverter = function(city) {
        return {
            id: city.id, 
            country_id: city.country_id, 
            name: city.name, 
            number_hotels: city.number_hotels
        };
    };
    
    var HotelConverter = function(hotel){
        return {
            id: hotel.id, 
			cityId: hotel.city_id,
			star: hotel.star,
			ratesFrom: hotel.rates_from,
            pictures: hotel.pictures,
            overview: hotel.overview,
            address: hotel.address,
            location: hotel.location,
            name: hotel.name,
            rooms: _.map(hotel.rooms, 
                function(room) {return {
                    id: room.id, 
                    name: room.name, 
                    max_occupancy: room.max_occupancy, 
                    max_extrabeds: room.max_extrabeds, 
                    picture: room.picture
                };})
        };
    };
    
    var hotelService = {
        getCountryList: function(callback) {
            Country.find({}, function(err, countries) {
                if (err) return callback(err);
                async.map(countries,
                    function(country, callback) {
                        callback(null, {
                            id: country.id,
                            name: country.name
                        });
                    },
                    function(err, results) {
                        return callback(null, results);
                    }
                );
            });
        },

        searchCityById: function(id, callback) {
            City.findById(id, function(err, city){
                if(err){
                    console.log(err);
                    return callback(null, null);  
                } 
                if(city) city = CityConverter(city);
                return callback(null, city);
            });
        },
        
        searchCityByKeywords: function(keyword, skip, limit, callback) {
            City.search(keyword, function(err, cities){
                if(err){
                    console.log(err);
                    return callback(null, null);  
                } 
                async.map(cities, 
                    function(city, callback){
                        callback(null, CityConverter(city));
                    }, 
                    function(err, result){
                        return callback(err, result);
                    }
                );
            });
        },
        
        searchHotelById: function(id, callback) {
            Hotel.findById(id, function(err, hotel){
                if(err){
                    logger.error(err);
                    return callback(null, null);
                }
                if(hotel) hotel = HotelConverter(hotel);
                return callback(null, hotel);
            });
        },
        
        searchHotelByKeywords: function(cityId, keywords, skip, limit, sort, callback){
            keywords = (keywords||'').toLowerCase();
            keywords = keywords.replace(/[\.,-]/g, " ");
            if(keywords.length > 0){
                keywords = _.unique(keywords.split(' '));
                keywords = _.filter(keywords, function(entry) { return entry.length > 0; });
                if(keywords.length === 0) keywords = null;
            }
            Hotel.search(cityId, keywords, skip, limit, sort, function(err, count, hotels){
                if(err){
                    console.log(err);
                    return callback(err);  
                } 
                async.map(hotels, 
                    function(hotel, callback){
                        callback(null, HotelConverter(hotel));
                    }, 
                    function(err, result){
                        return callback(err, count, result);
                    }
                );
            });
        },
        
        checkRate: function(hotelId, checkIn, checkOut, callback) {
            Hotel.findById(hotelId, function(err, hotel){
                if(err){
                    logger.error(err) ;
                } 
                if(!hotel) return callback('Hotel Not Found');
                
                var id = new mongoose.Types.ObjectId();
                
                async.parallel([
                    function(next){
                        callback(null, id);
                        return next();
                    },
                    function(next){
                        var command = {
                            id: id,
                            type: 'checkAvailability',
                            payload: {
                                hotelId: hotel.id,
                                meta: {
                                    agoda_id: hotel.meta.agoda_id
                                },
                                checkIn: checkIn,
                                checkOut: checkOut,
                                rooms: []
                            }
                        };
                        _(hotel.rooms).forEach(function(room){
                            command.payload.rooms.push({
                                id: room.id,
                                max_occupancy: room.max_occupancy,
                                max_extrabeds: room.max_extrabeds,
                                meta: {
                                    agoda_id: room.meta.agoda_id
                                }
                            });
                        });	
                        agodaAvailability.check(command, function(err, rate){
                            if(!err){
                                cache.set('HotelRate.' + rate.hotelRateId, JSON.stringify(rate));
	                            cache.expire('HotelRate.' + rate.hotelRateId, cacheTTL);
                            }
                        });
                    }
                ]);
			});
        },

        getRate: function(id, callback) {
	        cache.get('HotelRate.' + id, function (err, reply) {
		        if(err) return callback(err);
		        if(reply) {
			        console.log(reply);
			        //return callback(null, JSON.parse(reply));
			        var hotelRate = JSON.parse(reply);
			        var result = _.omit(hotelRate, 'rates');
		            result.rates = [];
		            _(hotelRate.rates).forEach(function(rate){
			            var tmp = _.omit(rate, 'promoId');
			            tmp.promo = (rate.promoId != null);
			            result.rates.push(tmp);	
		            });
		            return callback(null, result);
		        } else {
			        return callback('not found');                
		        }
            });
        },

        createBooking: function(command, callback) {
            async.waterfall([
                function(next){
                    cache.get('HotelRate.' + command.book.hotelRateId, function (err, reply) {
                        if(err) return next(err);
                        if(!reply) return next('HotelRate Not Found');
                        return next(null, JSON.parse(reply));
                    });
                },
                function(hotelRate, next){
                    console.log(hotelRate);
                    var booking = new Booking();
                    booking.hotelId = hotelRate.hotelId;
                    booking.checkIn = hotelRate.checkIn;
                    booking.checkOut = hotelRate.checkOut;
                    booking.agent = command.agent;
                    booking.guests = command.guests;        
                    booking.rooms = [];
                    booking.total = 0;
                    _(command.book.rooms).forEach(function(room){
                        var tmp = _.find(hotelRate.rates, function(rate) {return rate.roomRateId == room.roomRateId;});
                        if(!tmp) return false;
                        booking.rooms.push({
                            quantity: room.quantity,
                            roomId: tmp.roomId,
                            occupancy: tmp.occupancy,
                            extrabeds: tmp.extrabeds,
                            rate: tmp.rate,
                            promoId: tmp.promoId,
                            included: tmp.included,
                            cancellation: tmp.cancellation											
                        });						
                        booking.total = booking.total + tmp.rate.total * room.quantity;
                    });
                    
                    if(booking.rooms.length != command.book.rooms.length){
                        return next('Invalid Room Rate');
                    }
                    booking.save(next);	
                }],
                function(err, booking){
                    console.log(err);
                    console.log(booking);
                    if(err) return callback(err);
                    if(booking) return callback(null, booking.id);
                    return callback(null, null);
                }
            );
        },

        getBooking: function(id, callback) {
            Booking.findById(id, function(err, booking){
                if(err) return callback(err);
                return callback(null, {
                    id: booking.id,
                    hotelId: booking.hotelId,
                    checkIn: booking.checkIn,
                    checkOut: booking.checkOut,
                    rooms: booking.rooms,
                    guests: booking.guests,
                    agent: booking.agent,
                    status: booking.status,
                    merchant: booking.merchant,
                    total: booking.total
                });
            });
        },
        
        payBooking: function(id, callback) {
            //"status.code": "BUY_ONLY"
            Booking.lock(id, "BUY_ONLY", "BUY_PENDING", function(err, booking){
                if(!booking) return callback('Booking Not Found');
                console.log(JSON.stringify(booking));
                var command = {
	                bookingId: booking.id,
	                checkIn: booking.checkIn,
	                checkOut: booking.checkOut,
	                total: booking.total,
	                promoId: booking.rooms[0].promoId,
	                guests: [],	
	                //cityName: "ho chi minh",
	                //hotelId: "433223", 
	                //roomId: "1934496",
	                //quantity: 1,
	                //occupancy: 1,
	                //extrabeds: 0,
                };
                async.parallel([
                    function(next){
                        callback();
                        return next();
                    },
                    function(next){
                        console.log('Find hotel Info ' + booking.hotelId);
                        Hotel.findById(booking.hotelId, function(err, hotel){
                            if(err || !hotel) return next('Hotel Not Found');
                            command.hotelId = hotel.meta.agoda_id;
                            _(hotel.rooms).forEach(function(room){
                                if(room.id === booking.rooms[0].roomId){
                                    command.roomId = room.meta.agoda_id;
                                    command.occupancy = booking.rooms[0].occupancy;
                                    return false;
                                }
                            });
                            City.findById(hotel.city_id, function(err, city){
                                if(err || !city) return next('Hotel Not Found');
                                command.cityName = city.name.en;
                                return next(null);
                            }); 
                        });
                    },
                    function(next){
                        command.quantity = 0;
                        command.extrabeds = 0;
                        _(booking.rooms).forEach(function(room){
                            command.quantity = command.quantity + room.quantity;
                            command.extrabeds = command.extrabeds + room.extrabeds;
                        });
                        return next(null);
                    },
                    function(next){
                        async.each(
                            booking.guests, 
                            function(guest, next){
                                Country.findById(guest.nationality, function(err, country){
                                    if(err || !country) return next('nationality');
                                    command.guests.push({
                                        firstName: guest.firstName,
                                        lastName: guest.lastName,
                                        email: guest.email,
                                        mobile: guest.mobile,
                                        nationalId: country.meta.agoda_id
                                    });
                                    return next();
                                });     
                            }, 
                            function(err){
                                return next(err);
                            }
                        );
                    }],
                    function(err){
                        if(err){
                            console.log('Create PayCommand Error: ' + err);
                            booking.status.code = 'BUY_ERROR';
                            booking.status.description = err.toString();
                            booking.status.updated = new Date();
                            booking.save();
                        }else{
                            console.log('Submit PayCommand: ' + JSON.stringify(command));;
                            agodaPay.handle(command, function(err, res){
                                console.log('PayResult ' + res)
                                booking.status.code = res.code;
                                booking.status.reservationCode = res.reservationCode;
                                booking.status.description = res.msg;
                                booking.status.updated = new Date();
                                booking.save();
                            });
                        }
                        //console.log('err ' + err);
                        //console.log('command ' + JSON.stringify(command) +'\n');
                    }
                );
            });
        },
        
        voidBooking: function(callback) {
            
        }
    };
    
    
    register(null, {
        "hotelService": hotelService
    });
};
