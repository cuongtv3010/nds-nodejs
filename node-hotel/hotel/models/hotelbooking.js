var mongoose = require('mongoose'), 
	Schema = mongoose.Schema;
	
var bookingSchema = new Schema({
	status: {
        code: {type: String, default: 'BUY_ONLY'},    
        description: String,
        reservationCode: String,
		created: {type: Date, default: Date.now},
		updated: {type: Date, default: Date.now}
	},
	
	merchant: {type: String, default: 'Agoda'},
	
	agent: {
		id: String,
		mobile: String,
		email: String
	},
	
	hotelId: String,			
	checkIn: String,
	checkOut: String,
	
	guests: [{
		_id: false,
		firstName: String,
		lastName: String,
		email: String,
		mobile: String,
		nationality: String		
	}],
	
	total: Number,
	
	rooms: [{
		_id: false,
		quantity: Number,
		roomId: String,
		occupancy: Number,
		extrabeds: Number,
		promo: Boolean,
		promoId: String,
		rate: {
			daily: [{
				_id: false,
				date: String,
                exclusive: Number,
                tax: Number,
                fee: Number,
                inclusive: Number	
			}],
			total: Number			
		},
		included: String,
		cancellation: {
			summary: {
				en: String,
				vi: String
			},
			charges: [{
				_id: false,
				after: String,
                before: String,                
                amount: Number
            }]
		}
	}]	
});
bookingSchema.set('versionKey', false);

bookingSchema.statics.lock = function lock(id, oldStatus, newStatus, callback) {
    this.findOneAndUpdate(
        {"_id": id, "status.code": oldStatus}, 
        { "status.code": newStatus, "status.updated": new Date()}, 
        callback
    );
}

var HotelBooking = mongoose.model('hotelbooking', bookingSchema);
	
module.exports = HotelBooking;
