var mongoose = require('mongoose'), 
	Schema = mongoose.Schema;
	
var countrySchema = new Schema({
	_id: String,
	meta: {
		agoda_id: String,
		updated: {type: Date, default: Date.now}
	},	
	name: {
		en: String,
		vi: String
	}
});
countrySchema.set('versionKey', false);

countrySchema.virtual('id').get(function () {
  return this._id;
});
countrySchema.virtual('id').set(function (id) {
  this._id = id;
});


var Country = mongoose.model('Country', countrySchema);
	
module.exports = Country;