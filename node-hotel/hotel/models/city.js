var mongoose = require('mongoose'), 
	Schema = mongoose.Schema;

var citySchema = new Schema({
	meta: {
		agoda_id: String,
		keywords: [String],
		updated: {type: Date, default: Date.now}
	},	
	country_id: String,
	name: {
		en: String,
		vi: String
	},
	number_hotels: Number
});

citySchema.set('versionKey', false);

citySchema.statics.search = function search(name, callback) {
	// replace all invalid characters
  name = name.replace(/[|&;$%@"<>()+,]/gi, "");
  return this.where('meta.keywords', new RegExp(name, 'i')) 
  	.limit(20)
  	.sort({ number_hotels: -1 })
  	.exec(callback);
}

var City = mongoose.model('City', citySchema);

module.exports = City; 
