var redis = require("redis");	

//redis://redistogo:bcaac0bae38856d5fc7f768ef877c941@squawfish.redistogo.com:10180/
	
var cacheTTL = 30 * 60; //seconds
var cache = redis.createClient(10180, "squawfish.redistogo.com");
cache.auth("bcaac0bae38856d5fc7f768ef877c941");
cache.on("connect", function(){console.log('Redis Connected')});
cache.on("error", function (err) {
    console.log("error event - " + cache.host + ":" + cache.port + " - " + err);
});
	
exports.save = function(roomrates){
	cache.set('RoomRates.' + roomrates.hotelRateId, JSON.stringify(roomrates));
	cache.expire('RoomRates.' + roomrates.hotelRateId, cacheTTL);
	console.log(JSON.stringify(roomrates));
}

exports.find = function(id, callback){
	cache.get('RoomRates.' + id, function (err, reply) {
		if(err) return callback(err);
		if(reply) {
			console.log(reply);
			return callback(null, JSON.parse(reply));
		} else {
			return callback('not found');                
		}
    });
}