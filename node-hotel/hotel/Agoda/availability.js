var _ = require('lodash'),
	async = require('async'),
	moment = require('moment');
var builder = require('xmlbuilder');
var request = require('request');
var cheerio = require('cheerio');

var SETTINGS = require('./config').SETTINGS;
/*
	command = {
		id: String,
		type:
		payload: {
			hotelId,
			meta {
				agodaid
			},
			checkin,
			checkout,
			rooms [{
					id, 
					max_occupancy, 
					max_extrabeds, 
					meta {
						agoda_id
					}
			}]
		}
	}
*/

//Niko		286608		522753cb1ea2b3843a002d14	
//Halo 		230301
//Old Inn 	487930


exports.check = function(command, callback){
	console.log(JSON.stringify(command));
	
	var rooms = _.filter(command.payload.rooms, function(room) { return room.meta.agoda_id; });
	GetPriceList(
		command.payload.meta.agoda_id, 
		rooms, 
		command.payload.checkIn, 
		command.payload.checkOut,
		function(err, paxSettings, rates){
			if(err){
				console.log('CheckRoomRates ' + err);
			}
			var result = {
				hotelRateId: command.id,
				hotelId: command.payload.hotelId,
				checkIn: command.payload.checkIn, 
				checkOut: command.payload.checkOut,
				paxSettings: paxSettings,
				rates: []		
			};		
			var rateInd = 0;	
			_(rates).forEach(function(rate){
				rate.roomRateId = (rateInd++).toString();
				result.rates.push(rate);
			});	
			return callback(null, result);
			//roomRates.save(result);
		}
	);
};

function GetPriceList(hotelId, rooms, checkIn, checkOut, callback){
	var paxSettings = null,
		priceMatrix = {},
		maxOccupancy = 0;
	
	_(rooms).forEach(function(room){
		maxOccupancy = _.max([maxOccupancy, room.max_occupancy + room.max_extrabeds]);
	});
		
	async.each(
		_.range(1, maxOccupancy + 1),
		function(occupancy, next){
			CheckPrice(hotelId, occupancy, checkIn, checkOut, function(err, res){
				console.log(res);
				if(!res) return next();
				if(!paxSettings) paxSettings = res.paxSettings;
			
				if(!res.priceOptions) return next();
				
				_(res.priceOptions).forEach(function(option){
					if(!priceMatrix[option.id]) priceMatrix[option.id] = {};
					var promo = option.promotionId || 'std';
					if(!priceMatrix[option.id][promo]) priceMatrix[option.id][promo] = [];
					priceMatrix[option.id][promo][occupancy] = option;
				});			
				return next();
			});	
		},
		function(err){
			var result = [];			
			_(rooms).forEach(function(room){
				var roomId = room.meta.agoda_id;
				var roomOccupancy = room.max_occupancy;
				if(!priceMatrix[roomId]) return;
				_(priceMatrix[roomId]).forEach(function(promo){
					if(!promo[roomOccupancy]) return;
					_.range(1, maxOccupancy + 1).forEach(function(occupancy){
						if(!promo[occupancy]) return;
						if(occupancy < roomOccupancy && promo[occupancy].rate.total == promo[roomOccupancy].rate.total) return;
						var item = {
							roomId: room.id,
							remainingRooms: promo[occupancy].remainingRooms,
							promoId: promo[occupancy].promotionId,
							//occupancy: promo[occupancy].occupancy,
							rate: promo[occupancy].rate,
							included: promo[occupancy].included,
							cancellation: promo[occupancy].cancellation
						};
						if(promo[occupancy].occupancy > room.max_occupancy){
							item.occupancy = room.max_occupancy;
							item.extrabeds = promo[occupancy].occupancy - room.max_occupancy;
						}else{
							item.occupancy = promo[occupancy].occupancy;
							item.extrabeds = 0;
						}
						result.push(item);
					});
				});
			});
			console.log('CheckRoomRates ' + paxSettings);
			console.log('CheckRoomRates ' + result);
			return callback(null, paxSettings, result);
		}	
	);
}

 var CheckPrice = function(hotelId, adults, checkin, checkout, callback){
	var root = builder.create('AvailabilityRequestV2', {'version': '1.0', 'encoding': 'UTF-8'});
	root.att('xmlns' ,"http://xml.agoda.com");
	root.att('xmlns:xsi', "http://www.w3.org/2001/XMLSchema-instance");	
	root.att('siteid', SETTINGS.siteid);
	root.att('apikey', SETTINGS.apikey);
	root.att('async',false);
	root.att('waittime',5);
	root.ele('Type', '4');
	root.ele('Id', hotelId); 
	root.ele('Radius', '0');
	root.ele('Latitude', '0');
	root.ele('Longitude', '0');
	root.ele('CheckIn', checkin);
	root.ele('CheckOut', checkout);
	root.ele('Rooms', '1');
	root.ele('Adults', adults.toString());
	root.ele('Children', '0');
	root.ele('Rateplan', 'Retail');
	root.ele('Language', 'vi-vn');
	root.ele('Currency', SETTINGS.currency);
	
	var xmlBody = root.end();	
	request(
		{
			method: 'POST',
			url: 'http://xml.agoda.com/xmlpartner/XmlService.svc/search_lrv2',
			headers: {"Content-Type" : "application/xml"},
			body: xmlBody				
		}, 
		function (error, response, body) {	
			//console.log(body);		
			if(error || response.statusCode != 200) return callback('CheckPrice Response Error');
			
			var $ = cheerio.load(body);
			console.log($);
			var result = {};
			
			var paxSettingsTag = $('PaxSettings');
			result.paxSettings = {
				submit: paxSettingsTag.attr('submit'),
				infantage: paxSettingsTag.attr('infantage'),
				childage: paxSettingsTag.attr('childage')
			}
			
			console.log(paxSettingsTag);
			console.log(result.paxSettings);
			
			result.priceOptions = [];
			$('Room').each(function() {
				var priceOption = {};				
				priceOption.id = this.attr('id');				
				priceOption.promotionId = this.attr('promotionid');
				priceOption.occupancy = adults;
				priceOption.remainingRooms = parseInt($("RemainingRooms", this).text(), 10);				
				priceOption.rate = {
					daily: [],
					total: 0
				};
				
				$('RateInfo DailyRates DailyRateDate', this).each(function(){
					var dailyRate = {
						date: this.attr('date'),
						exclusive: parseInt(this.attr('exclusive'), 10),
						tax: parseInt(this.attr('tax'), 10),
						fee: parseInt(this.attr('fees'), 10),
						inclusive: parseInt(this.attr('inclusive'), 10)						
					}
					priceOption.rate.daily.push(dailyRate);
					priceOption.rate.total = priceOption.rate.total +  dailyRate.inclusive;
				});
							
								
				priceOption.included = $('RateInfo Included', this).text();
				var cancellation = {};
				cancellation.summary = {
					en: $('Cancellation PolicyText[language=en-us]', this).text(),
					vi: $('Cancellation PolicyTranslated[language=vi-vn]', this).text()
				}
				
				var charges = [];
				$('Cancellation PolicyParameters PolicyParameter', this).each(function(){
					var charge = {
						days: parseInt(this.attr('days'), 10),
						type: this.attr('charge'),
						value: parseInt(this.text(), 10)
					};
					if(charge.type == 'P'){
						charge.amount = priceOption.rate.total * charge.value / 100;
					}
					
					if(charge.type == 'N'){
						var amount = 0;
						_(priceOption.rate.daily).forEach(function(dailyRate){
							if(moment(dailyRate.date, "YYYY-MM-DD").diff(moment(checkin, "YYYY-MM-DD"), 'days') < charge.value){
								amount = amount + dailyRate.inclusive;
							} 
						});
						charge.amount = amount;
					}											
					charges.push(charge);
				});
				
				charges = _.sortBy(charges, function(charge) { return 0-charge.days; });
				_(charges).forEach(function(charge){
					charge.date = moment(checkin, "YYYY-MM-DD").subtract('days', charge.days);					
				});
				
				cancellation.charges = [];
				if(charges.length > 0){
					if(moment().isBefore(charges[0].date)){
						cancellation.charges.push({
							before: charges[0].date.format("YYYY-MM-DD"),
							amount: 0
						});
					}
					for(var i = 0, len = charges.length; i < len; i++){
						var entry = {};
						if(moment().isBefore(charges[i].date))
							entry.after = charges[i].date.format("YYYY-MM-DD");
						if(charges[i+1])
							entry.before = charges[i+1].date.format("YYYY-MM-DD");
						entry.amount = charges[i].amount;
						cancellation.charges.push(entry);
					} 
				}
				
				
				
				priceOption.cancellation = cancellation;
				console.log(priceOption);
				result.priceOptions.push(priceOption);
				
			});
			return callback(null,result);
		}
	);
}
