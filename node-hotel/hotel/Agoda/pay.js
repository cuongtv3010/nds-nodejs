var cheerio = require('cheerio');
var _ = require('lodash');
var moment = require('moment');
var async = require('async');
var url = require('url');
var zlib = require('zlib');

var CONFIG = require('./config').config;
var mbvCreditCard = require('./config').mbvCreditCard;

var request = require('request').defaults({
    headers: {
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20100101 Firefox/21.0',
        'Accept-Language': 'en-US,en',
        'Accept': 'text/html,application/xhtml+xml,application/xml'
    },
    followRedirect: false,
    agent: false
});


var gzipRequest = function(options, callback) {
    options.headers = options.headers || {};
    options.headers['Accept-Encoding'] = 'gzip';
    var req = request(options);

    req.on('response', function(res) {
        var chunks = [];
        res.on('data', function(chunk) {
            chunks.push(chunk);
        });

        res.on('end', function() {
            var buffer = Buffer.concat(chunks);
            var encoding = res.headers['content-encoding'];
            if (encoding == 'gzip') {
                zlib.gunzip(buffer, function(err, decoded) {
                    console.log('gzip');
                    callback(err, res, decoded && decoded.toString());
                });
            }
            else if (encoding == 'deflate') {
                zlib.inflate(buffer, function(err, decoded) {
                    callback(err, res, decoded && decoded.toString());
                })
            }
            else {
                callback(null, res, buffer.toString());
            }
        });
    });
    req.on('error', function(err) {
        callback(err);
    });
}

    function GetBookingPage(command, callback) {
        var cookie = request.jar();
        var checkIn = moment(command.checkIn);
        var checkOut = moment(command.checkOut);
        var nights = checkOut.diff(checkIn, 'days');

        async.waterfall([

        function(next) {
            console.log('GetBookingPage_1')
            gzipRequest({
                url: 'http://www.agoda.vn/partners/SearchBox/SearchResult.aspx',
                qs: {
                    Room: 1,
                    Adult: 2,
                    Children: 0,
                    Search: command.cityName,
                    Night: nights,
                    ArrivalDate: checkIn.format("MMMM/DD/YYYY"),
                    DepartDate: checkOut.format("MMMM/DD/YYYY"),
                    LanguageID: 1,
                    HotelID: command.hotelId,
                    Header: '',
                    Footer: '',
                    CID: CONFIG.siteId,
                    CurrencyCode: CONFIG.currency.vnd,
                    radius: 36,
                    cityid: 0
                },
                jar: cookie
            },

            function(err, response, body) {
                /*	
						response.headers["location"] = /pages/agoda/default/WaitPage.aspx?asq=vl3HLt1awvZNTL1CSbbBiuL5CVqUffN78LbXi7rEVEuoMVGR2boQHeRjQoqHiUrb%2bKZvD%2bDuQ%2f%2fvirWxBEs5r8MFr2lGvPZJ8UHfTL3XT4Pi9gFJ3zoRUUxA1bXicT8i&header=&footer=&providerID=332&HotelID=286608&cid=1606990&waitpage=hotel						
					*/
                if (err) return next('GetBookingPage_1_Request_Error');
                if (response.headers.location && response.headers.location.indexOf("/pages/agoda/default/WaitPage.aspx") >= 0) {
                    return next(null, 'http://www.agoda.vn' + response.headers.location);
                }
                
                return next('GetBookingPage_1_Response_Error', null);
            });
        },

        function(waitPageUrl, next) {
            console.log('GetBookingPage_2\n' + waitPageUrl);
            gzipRequest({
                url: waitPageUrl,
                jar: cookie
            },

            function(err, response, body) {
                /*
						/asia/vietnam/ho_chi_minh_city/hotel_nikko_saigon.html?asq=DJ1QznLBvaCBTWZAyZPAfF9E1J%2fpzYcB85FLMyjROgRFNm4kKRi3Hp%2b69NJzGeBpQkg8L%2bQg8WX5WW6o9dwWmo1mLk9dbC3dJZ5tJtehqjDoc6YRaq0xzBIhCPAZEYwHpgSU8AP%2fr9Ig6Tjfuh7JfopIQBVbK6QTjn9uy9aephIBfkQMcR01gpgCYkA8fDMJI2fd1eALkpz7mD13cP8Vv%2btdXcdfOFMAtEA6X%2fDbuzA6A7XufvSm8IxMsj01fli6Wl2uUbUpvlT6JoNRN5hjd1Qi3K06bGb7g6ErPhnOtgY%3d						
					*/
                if (err) return next('GetBookingPage_2_Request_Error');
                if (response.headers.location) {
                    console.log(response.statusCode);
                    return next(null, 'http://www.agoda.vn' + response.headers.location);
                }
                return next('GetBookingPage_2_Response_Error', null);
            });
        },

        function(hotelUrl, next) {
            console.log('GetBookingPage_3\n' + hotelUrl);
            gzipRequest({
                url: hotelUrl,
                jar: cookie
            },

            function(err, response, body) {
                /*
					/asia/vietnam/ho_chi_minh_city/hotel_nikko_saigon.html?asq=DJ1QznLBvaCBTWZAyZPAfF9E1J%2fpzYcB85FLMyjROgRFNm4kKRi3Hp%2b69NJzGeBpQkg8L%2bQg8WX5WW6o9dwWmo1mLk9dbC3dJZ5tJtehqjDoc6YRaq0xzBIhCPAZEYwHpgSU8AP%2fr9Ig6Tjfuh7JfopIQBVbK6QTjn9uy9aephIBfkQMcR01gpgCYkA8fDMJI2fd1eALkpz7mD13cP8Vv%2btdXcdfOFMAtEA6X%2fDbuzA6A7XufvSm8IxMsj01fli6Wl2uUbUpvlT6JoNRN5hjd1Qi3K06bGb7g6ErPhnOtgY%3d						
				*/
                if (err) return next('GetBookingPage_3_Request_Error');
                if (response.headers.location) {
                    console.log(response.statusCode);
                    return next(null, response.headers.location);
                }
                return next('GetBookingPage_3_Response_Error', null);
            });
        },

        function(hotelUrl, next) {
            console.log('GetBookingPage_4\n' + hotelUrl);

            gzipRequest({
                url: hotelUrl,
                jar: cookie
            },

            function(err, response, body) {
                if (err) return next('GetBookingPage_4_Request_Error');

                var content = cheerio.load(body);

                var roomUrl;
                var roomPricesBox = content('div#ctl00_ctl00_MainContent_ContentMain_RoomTypesListGrid_AB1771_upRoomTypes');

                _(_.range(100)).forEach(function(ind) {
                    var roomId = 'a#ctl00_ctl00_MainContent_ContentMain_RoomTypesListGrid_AB1771_rptRateContent_ctl' + (ind < 10 ? '0' : '') + ind + '_lblRoomNameLink';

                    roomId = content(roomId, roomPricesBox);

                    if (roomId.length != 1) return false;

                    roomId = url.parse(roomId.attr('href'), true).query.rmtID;

                    if (roomId != command.roomId) return true;

                    // Number of occupancy
                    var occupancy = content('a#button_room' + ind, roomPricesBox);

                    if (occupancy.length != 1) return false;

                    var preBookLink = occupancy.attr('href');

                    occupancy = parseInt(occupancy.attr('occupancy'), 10);

                    if (occupancy != command.occupancy) return true;

                    // Extra beds
                    var extrabeds = content('a#lnkExtrabed' + ind, roomPricesBox).length !== 0;

                    if (command.extrabeds != 0 && extrabeds == false) return true;

                    // Promotion or Flash sales
                    var promo = 'td#ctl00_ctl00_MainContent_ContentMain_RoomTypesListGrid_AB1771_rptRateContent_ctl' + (ind < 10 ? '0' : '') + ind + '_tdRoom1 table.room_pro';

                    promo = content(promo, roomPricesBox).length == 1;

                    var flash = 'td#ctl00_ctl00_MainContent_ContentMain_RoomTypesListGrid_AB1771_rptRateContent_ctl' + (ind < 10 ? '0' : '') + ind + '_tdRoom1 table.room_flash';

                    flash = content(flash, roomPricesBox).length == 1;
                    
                    if ((promo == true || flash == true) && !command.promoId) return true;

                    if ((promo == false && flash == false) && command.promoId) return true;
                    
                    // Room Url
                    roomUrl = 'http://www.agoda.com' + preBookLink;
                    console.log('selected {' + '\n\troomId: ' + roomId + '\n\toccupancy: ' + occupancy + '\n\textrabeds: ' + extrabeds + '\n\tpromo: ' + promo + '\n\tlink: ' + roomUrl);
                    return false;
                });

                if (roomUrl) return next(null, roomUrl);
                return next('Room Not Found');

            });
        },

        function(roomUrl, next) {
            console.log('GetBookingPage_5\n' + roomUrl);
            gzipRequest({
                url: roomUrl,
                qs: {
                    room: command.quantity
                },
                jar: cookie
            },

            function(err, response, body) {
                if (err) return next('GetBookingPage_5_Request_Error');

                var content = cheerio.load(body);

                var url = content('form[name=go2book]').attr('action');
                var arg = content('input[name=arg]').attr('value');

                return next(null, url, arg);
            });
        },

        function(bookUrl, arg, next) {
            console.log('GetBookingPage_6\n' + bookUrl);

            gzipRequest({
                url: bookUrl,
                method: 'POST',
                form: {
                    arg: arg
                },
                jar: cookie
            },

            function(error, response, body) {
                if (error) return next('GetBookingPage_6_Request_Error');
                var args = {};
                var domain = url.parse(bookUrl, false);
                args.domain = [domain.protocol, domain.host].join('//');

                var content = cheerio.load(body);
                var p = content('a#bestprice').attr('href');
                if (p) {
                    p = url.parse(p, true);
                    p = p.query.p;
                }
                args.p = p;

                //extrabeds					
                args.extrabeds = [];
                content('input[id^=ContentPlaceHolder1_chklstExtraBed_]').each(function() {
                    args.extrabeds.push(this.attr('value'));
                });

                //
                var is1712 = false;
                content('script').each(function() {
                    var scriptText = this.html();
                    if (scriptText.indexOf("var is1712= true;") >= 0) {
                        is1712 = true;
                    }
                    var match = (/var chargedate = '(\d{4}\/\d{2}\/\d{2})'/g).exec(scriptText);
                    if (match && match[1]) {
                        args.chargeDate = new Date(match[1]);
                    };
                });
                if (is1712 === false || !args.chargeDate) {
                    args.chargeDate = new Date();
                }

                return next(null, args);
            });
        }],

        function(err, args) {
            if (err) return callback(err);
            args.cookie = cookie;
            return callback(null, args);
            //return callback('Stop');
        });
    }

exports.handle = function(command, callback) {
    console.log(command);

    async.waterfall([

    function(next) {
        GetBookingPage(command, function(err, args) {
            //console.log(args);
            //return next('Stop');
            return next(err, args);
        });
    },

    function(args, next) {
        console.log('GetPaymentModel');
        request({
            url: args.domain + "/b/book.aspx/GetPaymentModel",
            qs: {
                p: args.p,
                nocache: new Date().getTime()
            },
            method: 'POST',
            json: {},
            jar: args.cookie
        },

        function(error, response, body) {
            if (error) return next('GetPaymentModel_Request_Error');
            args.paymentModel = body.d;

            //return next('Stop GetPaymentModel');
            return next(null, args);
        });
    },
    /*
		function(args, next){
			console.log('SetCustomerDetail');
			request(
				{ 
					url: args.domain + "/b/book.aspx/SetCustomerDetail",
					qs: {p: args.p, nocache: new Date().getTime()},
					method: 'POST',							
					json: {},
					jar: cookie
				},
				function(error, response, body){
					console.log(JSON.stringify(body));
					if(error) return next(err);
					return next(null, args);
				}
			);
		},*/
    /*
		function(args, next) {
			console.log('GetPaymentMethodList');
			request(
				{ 
					url: args.domain + "/b/book.aspx/GetPaymentMethodList",
					qs: {p: args.p, nocache: new Date().getTime()},
					method: 'POST',							
					json: {languageId : 1},
					jar: cookie
				},
				function(error, response, body){
					console.log(JSON.stringify(body));
					if(error) return next(err);
					return next(null, args);
				}
			);
		},
		*/
    function(args, next) {
        if (command.extrabeds === 0) return next(null, args);
        console.log('RequestExtraBed');
        if (args.extrabeds.length < command.extrabeds) return next('Invalid Command: Extrabeds');

        async.eachSeries(
        _.range(command.extrabeds),

        function(index, next) {
            request({
                url: args.domain + "/b/book.aspx/RequestExtraBedOnOccFreeRoom",
                qs: {
                    p: args.p,
                    nocache: new Date().getTime()
                },
                method: 'POST',
                json: {
                    roomId: args.extrabeds[index],
                    useExtrabed: true
                },
                jar: args.cookie
            },

            function(error, response, body) {
                if (error) return next('ExtraBed_Request_Error');
                return next();
            });
        },

        function(err) {
            if (err) return next(err);
            return next(null, args);
        });
    },

    function(args, next) {
        console.log('GetBookingDetail');
        request({
            url: args.domain + "/b/book.aspx/GetBookingDetail",
            qs: {
                p: args.p,
                nocache: new Date().getTime()
            },
            method: 'POST',
            json: {},
            jar: args.cookie
        },

        function(error, response, body) {
            if (error) return next('GetBookingDetail_Request_Error');
            
            var data = body.d || {};

            var finalPrice = data.FinalPrice || '0';
            finalPrice = finalPrice.replace(/[,\.]/g, "");
            finalPrice = parseInt(finalPrice, 10);
            
            console.log('Final Price : ' + finalPrice);
            
            if (Math.abs(finalPrice - command.total) >= 100) return next('Invalid Command: Final Price Not Match ' + finalPrice);

            var hotelDetails = data.HotelDetails || {};
            hotelDetails = hotelDetails[0] || {}
            var adults = hotelDetails.Adults || '0';
            adults = parseInt(adults, 10);

            if (adults !== (command.occupancy * command.quantity + command.extrabeds)) return next('Invalid Command: Total Beds Not Match');
            return next(null, args);
            //return next('Stop');
        });
    },

    function(args, next) {
        console.log('SubmitFirstStep');
        request({
            url: args.domain + "/b/book.aspx/SubmitFirstStep",
            qs: {
                p: args.p,
                nocache: new Date().getTime()
            },
            method: 'POST',
            json: {
                // Agoda additional info, fcuk
                isswitchname: false,
                
                Iscsteuro : false,
                // smoking room
                is2152 : false,

                // Must have
                guests: JSON.stringify({
                    /*
                    {"guests":[{"Title":"","FirstName":"Hieu","LastName":"Nguyen","NationalId":"38"}]}
                   */
                    guests: [{
                        Title: "",
                        FirstName: command.guests[0].firstName,
                        LastName: command.guests[0].lastName,
                        NationalId: command.guests[0].nationalId //vietnam
                    }]
                }),
                /*
                    "{"ArrivalFlightNo":"","DepartFlightNo":"","IsNonSmoke":false,"LateCheckIn":false,
                        "EarlyCheckIn":false,"HighFloor":false,"LargeBed":false,
                        "TwinBeds":false,"AirportTransfer":"","AdditionalNotes":""}"
                */
                specialneed: JSON.stringify({
                    ArrivalFlightNo: "",
                    DepartFlightNo: "",
                    IsNonSmoke: false,
                    LateCheckIn: false,
                    EarlyCheckIn: false,
                    HighFloor: false,
                    LargeBed: false,
                    TwinBeds: false,
                    AirportTransfer: "",
                    AdditionalNotes: ""
                }),
                /*
                     {"FirstName":"Hieu","LastName":"Nguyen","CountryId":"38","Email":"hieu.nguyen@mobivi.com",
                        "ConfirmEmail":"hieu.nguyen@mobivi.com","PhoneNo":"0968356408","Newsletter":true,
                        "Address1":"","Address2":"","City":"","Postal":"","MembershipContentText":""}"
                */
                customer: JSON.stringify({
                    FirstName: command.guests[0].firstName,
                    LastName: command.guests[0].lastName,
                    CountryId: command.guests[0].nationalId,
                    Email: command.guests[0].email,
                    ConfirmEmail: command.guests[0].email,
                    PhoneNo: command.guests[0].mobile,
                    Newsletter: false,
                    Address1: "",
                    Address2: "",
                    City: "",
                    Postal: "",
                    MembershipContentText: ""
                })
            },
            jar: args.cookie
        },

        function(error, response, body) {
            if (error) return next('SubmitFirstStep_Request_Error');
            
            if (!body.d || body.d.ErrorList.length !== 0) return next('SubmitFirstStep_Response_Error');
            return next(null, args);
            //return next('Stop');
        });
    },

    function(args, next) {
        console.log('Submitbooking');
        // setting chargeDate for creditCard for testing. 
        mbvCreditCard.FullyChargeDate = args.chargeDate; // remove it after production phase
        
        request({
            url: args.domain + "/b/book.aspx/SubmitStep",
            qs: {
                p: args.p,
                nocache: new Date().getTime()
            },
            method: 'POST',
            json: {
                // smoking room
                is2152 : false,
                
                guests: JSON.stringify({
                    guests: [{
                        Title: "",
                        FirstName: command.guests[0].firstName,
                        LastName: command.guests[0].lastName,
                        NationalId: command.guests[0].nationalId //vietnam
                    }]
                }),
                specialneed: JSON.stringify({
                    ArrivalFlightNo: "",
                    DepartFlightNo: "",
                    IsNonSmoke: false,
                    LateCheckIn: false,
                    EarlyCheckIn: false,
                    HighFloor: false,
                    LargeBed: false,
                    TwinBeds: false,
                    AirportTransfer: "",
                    AdditionalNotes: ""
                }),
                customer: JSON.stringify({
                    FirstName: command.guests[0].firstName,
                    LastName: command.guests[0].lastName,
                    CountryId: command.guests[0].nationalId,
                    Email: command.guests[0].email,
                    ConfirmEmail: command.guests[0].email,
                    PhoneNo: command.guests[0].mobile,
                    Newsletter: false,
                    Address1: "",
                    Address2: "",
                    City: "",
                    Postal: "",
                    MembershipContentText: ""
                }),
                creditcard: JSON.stringify(mbvCreditCard) // creditcard will be set to args attributes after testing
            },
            jar: args.cookie
        },

        function(error, response, body) {
            if (error) return next('SubmitFirstStep_Request_Error');
            if (!body.d || body.d.ErrorList.length !== 0) {
                return next('SubmitFirstStep_Response_Error (' + JSON.stringify(body.d.ErrorList) + ')');
            }
            return next(null, args);
        });
    },

    function(args, next) {
        console.log('CheckCurrency');
        request({
            url: args.domain + "/b/book.aspx/CheckCurrency",
            qs: {
                p: args.p,
                nocache: new Date().getTime()
            },
            method: 'POST',
            json: {
                isRedirect: false
            },
            jar: args.cookie
        },

        function(error, response, body) {
            if (error) return next('CheckCurrency_Request_Error');
            console.log('CheckCurrency Response: ' + JSON.stringify(body));
            return next(null, args);
            //return next('Stop');	
        });
    },

    function(args, next) {
        console.log('DoBook');
        request({
            url: args.domain + "/b/book.aspx/DoBook",
            qs: {
                p: args.p,
                nocache: new Date().getTime()
            },
            method: 'POST',
            json: {},
            jar: args.cookie
        },

        function(error, response, msg) {
            console.log(msg);
            var result = {
                code: 'BUY_UNKNOWN'
            }

            if (error) {
                result.code = 'BUY_UNKNOWN';
                result.msg = 'DoBook_Request_Error';
                return next(null, args, result);
            }

            console.log('DoBook : ' + JSON.stringify(msg));
            if (msg.d.StatusCode == 302 || msg.d.StatusCode == 425) {
                result.code = 'BUY_UNKNOWN';
                result.msg = 'DoBook_Unknown_Status ' + msg.d.NextUrl;
                return next(null, args, result);
            }

            if (msg.d.StatusCode == 200 || msg.d.StatusCode == 0) {
                args.finalLink = msg.d.NextUrl;
                result.code = 'BUY_SUCCESS';
                return next(null, args, result);
            }

            result.code = 'BUY_ERROR';
            result.msg = 'DoBook_Error_Status ' + msg.d.StatusCode;
            return next(null, args, result);
        });
    },

    function(args, result, next) {
        if (!args.finalLink) return next(null, result);
        console.log('Thankyou');
        console.log(args);
        request({
            url: args.domain + "/b/en-us/thankyou.html",
            qs: {
                p: args.p
            },
            jar: args.cookie
        },

        function(error, response, body) {
            if (error) {
                return next(null, result);
            }
            var content = cheerio.load(body);
            console.log(content('div.msgconfirm').html());
            result.reservationCode = content('div.msgconfirm strong').first().text();
            return next(null, result);
        });
    }],

    function(err, result) {
        if (err) {
            console.log(err.toString());

            return callback(null, {
                bookingId: command.bookingId,
                code: 'BUY_ERROR',
                msg: err.toString()
            });
        }
        else {
            result.bookingId = command.bookingId;
            return callback(null, result);
        }
    });
}
