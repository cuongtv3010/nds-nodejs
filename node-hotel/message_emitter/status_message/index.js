module.exports = function setup(options, imports, register){
	var bookingSchema = require('../../hotel/models/hotelbooking').schema;
	var eventBus = imports.eventBus;

//POST INIT MIDDLEWARE is called after a document is load from db
	
bookingSchema.post('init', function(book){

		book.previous = book.status.code; // store previous status
		console.log('old code = ' + book.previous);
	});
//SAVE MIDDLEWARE takes action after save() called
bookingSchema.post('save', function(book){
		if(book.previous){ // check if book is loaded from db
			console.log('new code = ' + book.status.code);
			if(book.previous.localeCompare(book.status.code) != 0){
				eventBus.emit('statusChanged', book);
			}
		}
		
		
	});
	register(null,null);
}
