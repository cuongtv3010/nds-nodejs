module.exports = function setup(options, imports, register){
	var amqp = require('amqp');
	var rabbitMQconfig = require('../../hotel/Agoda/config').rabbitMQconfig;
	var queueToSendTo = "statusQueue";
	var eventBus = imports.eventBus;
	eventBus.on('statusChanged', function(bookObj){
					console.log('changed in status detected');
					var message = {
							bookId:bookObj.id,
							previousStatus: bookObj.previous,
							currentStatus: bookObj.status.code
							};
					var connection = amqp.createConnection(rabbitMQconfig);
					connection.on('ready', function(){	
					console.log('connection ready');
						// publish the new message
							connection.publish(queueToSendTo, message);
						});
						
		});
	register(null,null);
}
