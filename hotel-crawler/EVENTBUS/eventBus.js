module.exports = function setup(options, imports, register){
	var util = require('util');
	var eventEmitter = require('events').EventEmitter;

	register(null,{
		eventBus:new eventEmitter()
	});
}
