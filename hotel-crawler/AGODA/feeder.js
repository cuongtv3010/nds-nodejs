var request = require('request');
var cheerio = require('cheerio');
var async = require('async');
var _ = require('lodash');
var qs = require('querystring');

var CONFIG = require('./config.js');

var requestQueue = async.queue(function(options,callback){
	request(options,function(error,response,body){
		if (!error && response.statusCode == 200){
			callback(null,body);
		}else{
			callback("Request + "+ options.url +" failed");
		}
	})
}, 3);

function queuedRequest (options,callback){
	requestQueue.push(options,callback)
}



module.exports  =  {
	getCountries : function(callback){
		var queryArgs = {
			feed_id: "2",
			apikey :  CONFIG.key,//"e3f60702-ae3b-4b96-b41d-1baba49ad734",
			olanguage_id : CONFIG.lang_vn//"24"
		}
		queuedRequest({url:CONFIG.url,qs:queryArgs},function(err,body){
			if (err) return callback(err);
			parseAgodaCountries (body,function(countries){
				return callback(null,countries);
			})
		})
	},
	getCitiesInCountry : function(country,callback){
		var queryArgs = {
			feed_id: "3",
			apikey : CONFIG.key,//"e3f60702-ae3b-4b96-b41d-1baba49ad734",
			ocountry_id: country.meta.agoda_id || 'none',
			olanguage_id : CONFIG.lang_vn//"24"
		}
		queuedRequest({url:CONFIG.url,qs:queryArgs},function(err,body){
			if (err) return callback("Can't connect to agoda webservice to get cities for countries "+ country.name.en + " ERROR:" + err);
			parseAgodaCities(body,function(cities){
				return callback(null,cities)
			})
		});
	},
	getHotelsInCity : function(city,callback){
		async.waterfall([
			function(next){
				var queryArgs = {
					feed_id: "5",
					apikey : CONFIG.key, //"e3f60702-ae3b-4b96-b41d-1baba49ad734",
					oCity_id: city.meta.agoda_id|| 'none',
					olanguage_id : CONFIG.lang_vn, //"24"
					ocurrency: CONFIG.currency
				}
				queuedRequest({url:CONFIG.url,qs:queryArgs},function(err,body){
					if (err) return next(err);
					parseAgodaHotelsForIdNameStarLocation(body,function(hotels){
						return next(null,hotels);
					})
				});	
			},
			function(hotels,next){
				var hotelResult = [];
				async.each(hotels,function(hotel,doneCallback){
					parseAgodaHotelInfo(hotel,function(err,hotelReturn){
						_.merge(hotel,hotelReturn);
						hotelResult.push(hotel)
						console.log("Done Hotel " + hotel.name.en);
						doneCallback();
					})
				},function(err){
					if (err) return next(err);
					return next(null,hotelResult);
				})
			}
		],function(err,hotelResult){
			if (err) return callback(err);
			return callback(null,hotelResult);

		})
	}
}



/*********************** XML Parse Function ****************************/
function parseAgodaCountries (xmlBody,callback){
	var $ = cheerio.load(xmlBody,{ignoreWhitespace: true, xmlMode: true});
	var countryArray = {};
	$('country').each(function(i,item){
		var country = {};
		$(item).children().each(function(i,item){
			var attributeName = $(this)[0].name;
			country[attributeName] = $(this).text();
		})
		countryArray[country.country_iso2] = {
			_id:country.country_iso2,
			meta:{
				agoda_id:country.country_id
			},
			name:{
				en: country.country_name,
				vi: country.country_translated
			}
		};
	})
	callback(countryArray);
}
	
function parseAgodaCities (xmlBody,callback){
	var $ = cheerio.load(xmlBody,{ignoreWhitespace: true, xmlMode: true});
	var cityArray = [];
	$('city').each(function(i,item){
		var city = {};
		$(item).children().each(function(i,item){
			var attributeName = $(this)[0].name;
			city[attributeName] = $(this).text();
		})
		cityArray.push({
			meta:{
				agoda_id:city.city_id,
			},
			name:{
				en:city.city_name,
				vi:city.city_translated
			}
		});
	})
	callback(cityArray);
}
	
function parseAgodaHotelsForIdNameStarLocation (xmlBody,callback){
	var $ = cheerio.load(xmlBody,{ignoreWhitespace: true, xmlMode: true,lowerCaseTags:true});
	var hotels = [];
	
	$('hotel').each(function(){
		var hotel = {};
		hotel.meta = {};
		hotel.name = {};
		hotel.location = {};
		hotel.meta.agoda_id = $(this).find('hotel_id').text();
		hotel.meta.rating = $(this).find('rating_average').text();
		hotel.rates_from = $(this).find('rates_from').text();
		hotel.name.en = $(this).find('hotel_name').text();
		hotel.name.vi = $(this).find('translated_name').text();
		hotel.star = $(this).find('star_rating').text();
		hotel.location.longitude = $(this).find('longitude').text();
		hotel.location.latitude = $(this).find('latitude').text();
		hotels.push (hotel);
	});
	callback(hotels);
}

function parseAgodaHotelInfo(hotel,callback){
	async.parallel([
		function(next){
			var queryArgs = {
				feed_id: "19",
				apikey :CONFIG.key,
				olanguage_id: CONFIG.lang_vn,
				ocurrency: CONFIG.currency,
				mhotel_id: hotel.meta.agoda_id
			}
			queuedRequest({url:CONFIG.url,qs:queryArgs},function(err,body){
				if (err) return next(err);
				parseAgodaHotelInfoVN(body,function(hotelResult){
					_.merge(hotel,hotelResult);
					return next(null);
				})
			})
		},
		function(next){
			var queryArgs = {
				feed_id : "19",
				apikey : CONFIG.key,
				ocurrency: CONFIG.currency,
				mhotel_id: hotel.meta.agoda_id
			}
			queuedRequest({url:CONFIG.url,qs:queryArgs},function(err,body){
				if (err) return next(err);
				parseAgodaHotelInfoEN(body,function(hotelResult){
					_.merge(hotel,hotelResult);
					return next(null);
				})
			})
		}
	],function(err,hotel){
		if (err) return callback(err);
		return callback(null,hotel);
	})
}

function parseAgodaHotelInfoVN (xmlBody,callback){
	var $ = cheerio.load(xmlBody);
	var hotel = {};
	hotel.pictures = [];
	$('picture').each(function(){
		hotel.pictures.push($(this).find('URL').text());
	})

	hotel.facilities = {};
	hotel.facilities.hotel = {};
	hotel.facilities.hotel.en = [];
	hotel.facilities.hotel.vi = [];
	hotel.address = {};

	$('facility').each(function(){
		hotel.facilities.hotel.en.push($(this).find('property_name').text());
		hotel.facilities.hotel.vi.push($(this).find('property_translated_name').text());
	});

	var index = 0;
	$('address_line_1').each(function(){
		if (index == 0) hotel.address.en = $(this).text();
		else hotel.address.local = $(this).text();
		++index;
	})

	hotel.overview = {};
	hotel.overview.vi = $('overview').text();

	hotel.rooms = [];
	$('roomtype').each(function(){
		var room = {};
		room.meta = {};
		room.name ={};
		room.max_extrabeds = $(this).find('max_extrabeds').text();
		room.max_occupancy = $(this).find('max_occupancy_per_room').text();
		room.meta.agoda_id = $(this).find('hotel_room_type_id').text();
		room.name.en=$(this).find('standard_caption').text();
		room.name.vi = $(this).find('standard_caption_translated').text();
		room.picture = $(this).find('hotel_room_type_picture').text();
		hotel.rooms.push(room);
	})
	return callback(hotel);
}


function parseAgodaHotelInfoEN (xmlBody,callback){
	var $ = cheerio.load(xmlBody);
	var hotel = {};
	hotel.overview = {};
	hotel.overview.en = $('overview').text();
	return callback(hotel);
}

