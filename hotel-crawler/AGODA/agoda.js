module.exports = function setup(options, imports, register){
	var feeder = require('./feeder.js');	
	var config = require('./config.js');
	
	config.lang_vn=options.configs.lang_vn;
	config.currency = options.configs.currency;
	config.siteId = options.configs.siteId;
	config.url = options.configs.url;
	config.key = options.configs.key;

    register(null,{
		agodaService:{
			merchant_id:"agoda_id",
			feeder: feeder,
		}
	})


	
}
