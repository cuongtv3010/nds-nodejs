var mongoose = require('mongoose'), 
	Schema = mongoose.Schema;
var hotelSchema = new Schema({
	meta: {
		agoda_id: String,
		//~ ivivu_id: String,
		keywords: [String],
		rating: Number,
		updated: {type: Date, default: Date.now}
	},
	name: {
		en: String,
		vi: String
	},
	star: Number,	
	city_id: String,
	facilities:{
		hotel:{
			en:[String],
			vi:[String]
		}
	},
	location: {
		longitude: Number,
		latitude: Number
	},
	rates_from: Number,
	address: {
		en: String,
		local: String
	},
	overview: {
		en: String,
		vi: String
	}, 	
	pictures: [String],	
	rooms: [{
		meta: {
			agoda_id: String,
			ivivu_id: String
		},
		name: {
			en: String,
			vi: String
		},
		max_occupancy: Number,
		max_extrabeds: Number,
		picture: String	
	}]
});

hotelSchema.statics.search = function search(cityId, keywords, skip, limit, sort, callback){
	var query = this.where('city_id', cityId);
	if(keywords){		
		query.where('meta.keywords',  new RegExp('(' + keywords.join('|')+ ')', 'i'));
	}
	
	query.count(function (err, count) {
		query.find();
		if(skip){
            if(skip >= count) return callback(null, count, []);
            query.skip(skip);
		}
		if(limit) query.limit(limit);
		
		switch(sort){
			case 'star_desc':  
				query.sort({'star': -1 });
                break;
			case 'star_asc':
				query.sort({'star': 1 });
				break;
			case 'rate_desc':
				query.sort({'rates_from': -1 });
				break;
			case 'rate_asc':
				query.sort({'rates_from': 1 });
				break;
			default:
                query.sort({'meta.rating': -1 });
		}
		query.exec(function(err, res){
			return callback(err, count, res);
		});	
	});
}
//~ hotelSchema.post('save', function(hotel){
		//~ console.log('-----------------------SAVED INTO MOnGO-------------');
		//~ console.log('DATE INSERTED = ' + hotel.meta.updated);
		//~ console.log('HOTEL NAME = ' + hotel.name);
		//~ console.log(hotel);
	//~ });
var Hotel = mongoose.model('Hotel', hotelSchema);
	
module.exports = Hotel;
