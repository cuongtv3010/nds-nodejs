var mongoose = require('mongoose'), 
	Schema = mongoose.Schema;

var citySchema = new Schema({
	meta: {
		agoda_id: String,
		ivivu_id: String,
		keywords: [String],
		updated: {type: Date, default: Date.now}
	},	
	country_id: String,
	name: {
		en: String,
		vi: String
	},
	number_hotels: Number
});

citySchema.set('versionKey', false);

citySchema.statics.search = function search(name,skip,limit, callback) {
  return this.where('meta.keywords', new RegExp(name, 'i'))
  	.skip(skip)
  	.limit(limit)
  	.sort({ number_hotels: -1 })
  	.exec(callback);
}
//~ citySchema.post('save', function(city){
		//~ console.log('-----------------------SAVED CITY INTO MONGO-------------');
		//~ console.log('DATE INSERTED = ' + city.meta.updated);
		//~ console.log('CITY NAME = ' + city.name.en);
		//~ console.log(city);
	//~ });
var City = mongoose.model('City', citySchema);

module.exports = City; 
