var architect = require("architect");
var mongoose = require('mongoose');
var mongoDBurl = require('./config').mongoDBurl;
var appConfig = require('./config').appConfig;
mongoose.connect(mongoDBurl,function(err,res){
	if (err){
		console.log("Connect failed " + err );
	}else{
		console.log("Connect success");
	}
})


var tree = architect.resolveConfig(appConfig, __dirname);
architect.createApp(tree, function (err, app) {
  if (err) throw err;
  console.log("app ready");
});
