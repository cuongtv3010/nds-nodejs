var architect = require("architect");

var mongoose = require('mongoose');
var urlString = 'mongodb://hoangnhathai:Aesx5099@ds043329.mongolab.com:43329/ivivu_crawl'
//var urlString = 'mongodb://haihoang88:Aesx5099@ds045679.mongolab.com:45679/mbv_hotel'
mongoose.connect(urlString,function(err,res){
	if (err){
		console.log("Connect failed " + err );
	}else{
		console.log("Connect success");
	}
})


var config = [
{
    packagePath: "./rest",
    port: process.env.PORT || 8080,
    host: "0.0.0.0" 
},
{
	packagePath: "./IVIVU",
	configs:{
		url :'http://vws2.ivivu.com:5555/api/search',
		key :'65af038dd19',
		eta : '1200',
        payment_card_number :'1234567890',
        payment_card_holder :'Hai Hoang',
        payment_card_expired_time : '0515',
        payment_card_code : '321'
	}
},
{
	packagePath: "./AGODA",
	configs:{
		lang_vn: "24",
		currency : "VND",
		siteId : "1606990",
		key: "e3f60702-ae3b-4b96-b41d-1baba49ad734",
		url: "http://xml.agoda.com/datafeeds/Feed.asmx/GetFeed",
        payment_card_number_1 :'1234',
        payment_card_number_2 :'1234',
        payment_card_number_3 :'1234',
        payment_card_number_4 :'1234',
        payment_card_holder   :'Hai Hoang',
        payment_card_expired_month : '05',
        payment_card_expired_year :'2017',
        payment_card_code :'321',
        payment_card_bank :'Vietcombank'
	}
},
'./Router',
{
	packagePath: "./hotel",
	configs:{
		redisPort: 9427,
		redisUrl: "angelfish.redistogo.com",
		redisKey: "23f013b6bdbb6b277f2f5bc5a8f3915f"
	}
},
'./EVENTBUS',
//'./InfoProvider'
];

var tree = architect.resolveConfig(config, __dirname);
architect.createApp(tree, function (err, app) {
  if (err) throw err;
  console.log("app ready");
});
