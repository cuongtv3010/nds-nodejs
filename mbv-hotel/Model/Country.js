var mongoose = require('mongoose'), 
	Schema = mongoose.Schema;
	
var countrySchema = new Schema({
	_id: String,
	meta: {
		agoda_id:String,
		ivivu_id: String,
		updated: {type: Date, default: Date.now}
	},	
	name: {
		en: String,
		vi: String
	}
});
countrySchema.set('versionKey', false);

countrySchema.virtual('id').get(function () {
  return this._id;
});
countrySchema.virtual('id').set(function (id) {
  this._id = id;
});

countrySchema.statics.findMostOutdatedCountry=function(callback){
	Country.findOne({},{},{sort:{"meta.updated": 1}},function(err,country){
			if (err) return callback(err);
			return callback(null,country);
		});
}

var Country = mongoose.model('Country', countrySchema);
	
module.exports = Country;