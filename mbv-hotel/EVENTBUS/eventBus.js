module.exports = function setup(options, imports, register){
	var util = require('util');
	var eventEmitter = require('events').EventEmitter;

	
	// var eventBus = function(){
	// 	var self = this;
	// 	this.on('hotel.avail.request',function(payload){
	// 		self.emit('router.avail.request',payload);
	// 	})

	// 	this.on('hotel.pay.request',function(payload){
	// 		self.emit('router.pay.request',payload);
	// 	})

	// 	this.on('info.feed.request',function(payload){
	// 		self.emit('router.feed.request',payload);
	// 	})

	// 	this.on('router.avail.response',function(payload){
	// 		self.emit('hotel.avail.response'+payload.result.hotelRateId, payload);
	// 	})

	// 	this.on('router.pay.response',function(payload){
	// 		console.log("Router return pay response " + JSON.stringify(payload));
	// 		self.emit('hotel.pay.response'+payload.result.bookingId,payload);
	// 	})

	// 	this.on('router.feed.response',function(payload){
	// 		self.emit('info.feed.response',payload);
	// 	})
	// }

	// util.inherits(eventBus,eventEmitter);

	register(null,{
		eventBus:new eventEmitter()
	});
}