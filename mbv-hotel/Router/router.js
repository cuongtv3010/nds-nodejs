
module.exports = function setup(options, imports, register){
	var _ = require('lodash');
	var async = require('async');

	//Init
    var merchants = [];
    merchants.push(imports.agodaService);
    merchants.push(imports.ivivuService);
    var eventBus = imports.eventBus;

    var doAvail = function(payload,callback){
		var command = payload.command;

	 	var merchant = _.find(merchants,function(correctMerchant){
	        return (command.payload.meta[correctMerchant.merchant_id]);
	    })
	    console.log("Router use " + merchant.name + " to serve avail command " + JSON.stringify(command));
	    merchant.availability.check(command,callback);
	}

	var doPay = function(payload,callback){
		var command = payload.command;
		var merchant = _.find(merchants,function(correctMerchant){
	        return (command.hotelMeta[correctMerchant.merchant_id]);
	    })

	    var merchant_id = merchant.merchant_id;
	    command.hotelId = command.hotelMeta[merchant_id];
	    delete command.hotelMeta;

	    _(command.guests).each(function(guest){
	        guest.nationalId = guest.nationalId[merchant_id];
	    })

	    command.roomId = command.roomId[merchant_id];

	    console.log("Router use " + merchant.name + " to serve pay command " + JSON.stringify(command));
	    merchant.pay.handle(command,callback);
	}

	var getCountries = function(callback){
		var countries = {};
	    async.forEachSeries(merchants,function(merchant,callback){
	        merchant.feeder.getCountries(function(err,returnCountries){
	            if (err) return callback(err);
	            _.merge(countries,returnCountries);
	            callback();
	        })
	    },function(err){
	        if (err) return callback(err);
	        return callback(null,countries);
	    })
	}

	var getCitiesInCountry = function(country,callback){
	    var cities = [];
	    async.forEachSeries(merchants,function(merchant,callback){
	        merchant.feeder.getCitiesInCountry(country,function(err,returnCities){
	            if (err) return callback(err);
	            _(returnCities).each(function(city){
	                city.merchant_id = merchant.merchant_id;
	            })
	            cities = cities.concat(returnCities);
	            callback();
	        })
	    },function(err){
	        if (err) return callback(err);
	        return callback(null,cities);
	    })
	}

	var getHotelsInCity = function(city,callback){
	    var hotels = [];
	    async.forEachSeries(merchants,function(merchant,callback){
	        merchant.feeder.getHotelsInCity(city,function(err,returnHotels){
	            if (err) return callback(err);
	            _(returnHotels).each(function(hotel){
	                hotel.merchant_id = merchant.merchant_id;
	            });
	            hotels = hotels.concat(returnHotels);
	            callback();
	        })
	    },function(err){
	        if (err) return callback(err);
	        return callback(null,hotels);
	    })
	}

	var doFeed = function(payload,callback){
		var type = payload.type;
		var argument = payload.argument;
		switch(type){
			case 'getCountries':
				getCountries(function(err,countries){
					return callback(err,countries);
				})
				break;
			case 'getCities':
				getCitiesInCountry(argument,function(err,cities){
					return callback(err,cities);
				})
				break;
			case 'getHotels':
				getHotelsInCity(argument,function(err,hotels){
					return callback(err,hotels);
				})

		}
	}

	//Waiting for incoming request
	eventBus.on('hotel.avail.request',function(payload){
		console.log("Router received avail request " + JSON.stringify(payload));
		doAvail(payload,function(err,result){
			eventBus.emit('hotel.avail.response',{error:err,result:result});
			//+payload.command.id
		})
	})

	eventBus.on('hotel.pay.request',function(payload){
		console.log("Router received pay request " + JSON.stringify(payload));
		doPay(payload,function(err,result){
			eventBus.emit('hotel.pay.response',{error:err,result:result});
			// +payload.command.bookingId
		})
	})

	eventBus.on('info.feed.request',function(payload){
		console.log("Router received feed request " + JSON.stringify(payload));
		doFeed(payload,function(err,result){
			eventBus.emit('info.feed.response',{error:err,result:result});
		})
	})

	register(null,null); 
}
