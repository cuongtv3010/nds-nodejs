var builder = require('xmlbuilder');
var moment = require('moment');
var request = require('request');
var cheerio = require('cheerio');
var qs = require('querystring');
var _ = require('lodash');
var async = require('async');
qs.escape = function(s){return s;}
var CONFIG = require('./config.js');
var availability = require('./availability.js');


// {
//     "bookingId": "5376d9d4bea5a62104c7eb03",
//     "checkIn": "2014-07-01",
//     "checkOut": "2014-07-05",
//     "total": 11947245,
//     "guests": [{
//         "firstName": "Hai",
//         "lastName": "Hoang",
//         "email": "hai.hoang@mobivi.com",
//         "mobile": "0933093320",
//         "nationalId": "38"
//     }],
//     "hotelId": "W53959",
//     "roomId": "127906",
//     "occupancy": 2,
//     "ratePlanId": "127906",
//     "bookingTarget": "W",
//     "cityName": "Siem Reap",
//     "quantity": 1,
//     "extrabeds": 1
// }


exports.handle = function(command,callback){
	async.waterfall([
		function(next){
			var availabilityCommand = {
				payload:{
					meta:{
						ivivu_id:command.hotelId
					},
					checkIn: command.checkIn,
					checkOut: command.checkOut,
					rooms : []
				}
			};
			var availabilityRoom = {
				max_occupancy: command.occupancy,
				max_extrabeds: command.extrabeds,
				meta:{
					ivivu_id: command.roomId
				}
			};
			availabilityCommand.payload.rooms.push(availabilityRoom);
			availability.check(availabilityCommand,function(err,result){
				if (err || !result) return next (err,{bookingId:command.bookingId,code:'BUY_ERROR',msg:"Can't retrieve ratePlanId and bookingTarget when making payment"});
				command.ratePlanId = result.rates[0].ratePlanId;
				command.bookingTarget = result.rates[0].bookingTarget;
				return next();
			})
		},
		function(next){
			var xml = generateXML(command);
			console.log(xml);
			request(
				{
					method: 'POST',
					url: 'https://vws2.ivivu.com/api/services/',
					headers: {"Content-Type" : "application/xml"},
					body: xml				
				}, 
				function (error, response,body){
					
					if (error || response.statusCode != 200){ 
						return next(error,{bookingId:command.bookingId,code:'BUY_ERROR',msg:"Can't connect to ivivu pay service"});
					}

					var $ = cheerio.load(body,{ignoreWhitespace: true, xmlMode: true,lowerCaseTags:true});
					var fault = $('faultstring');
					if (fault.length>0){
						return next(null,{bookingId:command.bookingId,code:'BUY_ERROR',msg:fault.text()});
					}

					var errors = $('ns2\\:Error');
					if (errors.length>0){
						var errorsString = "";
						errors.each(function(){
							errorsString = errorsString + $(this).attr('ShortText') + ' ';
						})
						return next(null,{bookingId:command.bookingId,code:'BUY_ERROR',msg:errorsString});
					}
                    var reservationCode = $('ns2\\:HotelReservationID').attr('ResID_Value');
                    if (!reservationCode) return next(null,{bookingId:command.bookingId,code:'BUY_UNKNOWN',msg:body});
	                return next(null,{bookingId:command.bookingId,code:'BUY_SUCCESS',reservationCode:reservationCode});
				}
			);
		}
	],function(err,res){
		return callback(err,res);
	})
	

}


function generateXML (command){
	var root = builder.create('soapenv:Envelope',{'version':'1.0'});
	root.att('xmlns:soapenv', 'http://schemas.xmlsoap.org/soap/envelope/');
	root.att('xmlns:ns','http://www.opentravel.org/OTA/2003/05');
	root.ele('soapenv:Header');
	var soapenvBody = root.ele('soapenv:Body');
	var OTA_HotelResRQ = soapenvBody.ele('ns:OTA_HotelResRQ');
	OTA_HotelResRQ.att('xmlns','http://www.opentravel.org/OTA/2003/05');
	OTA_HotelResRQ.att('xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
	OTA_HotelResRQ.att('xsi:schemaLocation','http://www.opentravel.org/OTA/2003/05');
	OTA_HotelResRQ.att('Version','1.003');

	var now = moment(new Date());
	var timeString = now.format('YYYY-MM-DDTHH:mm:ss.0Z');
	OTA_HotelResRQ.att('TimeStamp',timeString);
	OTA_HotelResRQ.att('Target','Test');

	var Source = OTA_HotelResRQ.ele('ns:POS').ele('ns:Source');
	var RequestorID = Source.ele('ns:RequestorID');
	RequestorID.att('Type','13');
	RequestorID.att('ID_Context','iVIVU Sales Booking API')
	RequestorID.att('ID','TMG')
	var BookingChannel = Source.ele('ns:BookingChannel');
	BookingChannel.att('Type','7');
	BookingChannel.ele('ns:CompanyName','ivivu.com');
	var HotelReservation = OTA_HotelResRQ.ele('ns:HotelReservations').ele('ns:HotelReservation');
	var RoomStay = HotelReservation.ele('ns:RoomStays').ele('ns:RoomStay');
	RoomStay.att('IndexNumber','1');
	RoomStay.ele('ns:RoomTypes').ele('ns:RoomType').att("Quantity",command.quantity).att("RoomID",command.roomId);
	RoomStay.ele('ns:RatePlans').ele('ns:RatePlan').att("RatePlanID",command.ratePlanId);
	RoomStay.ele('ns:GuestCounts').ele('ns:GuestCount').att('AgeQualifyingCode','10').att('Count',command.occupancy+command.extrabeds);
	RoomStay.ele('ns:TimeSpan').att('Start',command.checkIn).att('End',command.checkOut);
	RoomStay.ele('ns:BasicPropertyInfo').att('HotelCode',command.hotelId);
	var ResGuest = HotelReservation.ele('ns:ResGuests').ele('ns:ResGuest');
	ResGuest.att('PrimaryIndicator','true');
	ResGuest.att('ResGuestRPH','1');
	var Customer = ResGuest.ele('ns:Profiles').ele('ns:ProfileInfo').ele('ns:Profile').ele('ns:Customer');
	var PersonName = Customer.ele('ns:PersonName');
	PersonName.ele('ns:GivenName',command.guests[0].firstName);
	PersonName.ele('ns:Surname',command.guests[0].lastName);
	Customer.ele('ns:Telephone').att('PhoneNumber',command.guests[0].mobile);
	Customer.ele('ns:Email',command.guests[0].email);
	var ResGlobalInfo = HotelReservation.ele('ns:ResGlobalInfo');
	ResGlobalInfo.ele('ns:SpecialRequests').ele('ns:SpecialRequest').att('Name','ETA').ele('ns:Text',CONFIG.eta);
    var PaymentCard = ResGlobalInfo.ele('ns:Guarantee').att('GuaranteeType','PrePay').ele('ns:GuaranteesAccepted').ele('ns:GuaranteeAccepted').ele('ns:PaymentCard');
	PaymentCard.att('CardType','3').att('CardCode','VI').att('CardNumber',CONFIG.payment_card_number).att('ExpireDate',CONFIG.payment_card_expired_time).att('SeriesCode',CONFIG.payment_card_code);
	PaymentCard.ele('ns:CardHolderName',CONFIG.payment_card_holder);
	ResGlobalInfo.ele('ns:Total').att('AmountAfterTax',command.total).att('AdditionalFeesExcludedIndicator','true').att('CurrencyCode','VND');
	HotelReservation.ele('ns:TPA_Extensions').ele('ns:BookingAdapterTarget',command.bookingTarget);
	var xml = root.end();

	return xml;
}