
var cheerio = require('cheerio');
var async = require('async');
var _ = require('lodash');
var request= require('request');
var qs = require('querystring');
var moment = require('moment');
qs.escape = function(s){return s;}

var CONFIG = require('./config.js');

exports.check = function (command,callback){
	async.waterfall([
		function(next){
			var max_occupancy = 0;
			_(command.payload.rooms).each(function(room){
				var room_occupancy = room.max_occupancy + room.max_extrabeds;
				if (max_occupancy < room_occupancy) max_occupancy = room_occupancy;
			});
			var priceOptionsLists = [];
			var checkOut = moment(command.payload.checkOut,"YYYY-MM-DD");
			checkOut.subtract('days','1');
			checkOut = checkOut.format('YYYY-MM-DD')
			async.forEachSeries(_.range(1,max_occupancy+1),function(occupancy,callback){
				checkPrice(command.payload.meta.ivivu_id,occupancy,command.payload.checkIn,checkOut,function(err,priceOptions){
					if (err) return callback(err);
					priceOptionsLists = priceOptionsLists.concat(priceOptions);
					return callback()
				})
			},function(err){
				if (err) return next(err);
				return next(null,priceOptionsLists);
			});
		},
		function(priceOptionsList,next){
			var result = [];
			_(command.payload.rooms).each(function(room){
				var options = _.filter(priceOptionsList,function(priceOption){
					return priceOption.roomId == room.meta.ivivu_id;
				});

				var includedOption = _.find(options,function(option){
					return option.occupancy == room.max_occupancy;
				})

				_(options).each(function(option){
					if (option.occupancy < includedOption.occupancy && option.rate.total == includedOption.rate.total) return;
					var item = {
						roomId: room.id,
						remainingRooms:5,
						ratePlanId : option.ratePlanId,
						bookingTarget : option.bookingTarget,
						rate: option.rate,
						childAge: option.childAge,
						included: option.included, 
						cancellation : option.cancellation
					}
					if (option.occupancy > room.max_occupancy){
						item.occupancy = room.max_occupancy;
						item.extrabeds = room.max_extrabeds;
					}else{
						item.occupancy = option.occupancy;
						item.extrabeds = 0;
					}
					result.push(item);

				})
			})
			return next(null,result);
		},
		function(rates,next){
			var result = {
				hotelRateId: command.id,
				hotelId: command.payload.hotelId,
				checkIn: command.payload.checkIn, 
				checkOut: command.payload.checkOut,
				paxSettings: {
					submit: "one"
				},
				rates: []		
			};		
			var rateInd = 0;
			var childAge;
			_(rates).forEach(function(rate){
				rate.roomRateId = (rateInd++).toString();
				childAge = rate.childAge;
				delete rate.childAge;
				result.rates.push(rate);
			});
			if (childAge) result.paxSettings.childage = childAge;
			return next(null,result);
		}
	],function(err,result){
		if (err) return callback(err,{hotelRateId: command.id});
		return callback(null,result);
	})
}


var checkPrice = function(hotelId,occupancy,checkIn,checkOut,callback){
	async.waterfall([
		function(next){
			
			checkPriceBeforeTax(hotelId,occupancy,checkIn,checkOut,function(err,priceOptions){
				if (err) return next(err);
				return next(null,priceOptions);
			})

		},
		function(priceOptionsBeforeTax,next){
			checkPriceAfterTax(hotelId,occupancy,checkIn,checkOut,priceOptionsBeforeTax,function(err,priceOptionsAfterTax){
				if (err) return next(err);

				_(priceOptionsAfterTax).each(function(priceOption){
					var total = 0;
					_(priceOption.rate.daily).each(function(rate){
						total += rate.inclusive;
					})
					priceOption.rate.total = total;
				})

				return next(null,priceOptionsAfterTax);
			})
		}
	],function(err,priceOptions){
		if (err) return callback(err);
		return callback(err,priceOptions);
	})
}

function checkPriceBeforeTax (hotelId,occupancy,checkIn,checkOut,callback){
	//var urlString = "http://vws2.ivivu.com:5555/api/search/?param=inventory/dealmatrix/property/"+hotelId + "/rates;7&apiKey=65af038dd19?";
	var matrixParams = {
		param: "inventory/dealmatrix/property/"+hotelId + "/rates",
		apiKey: CONFIG.key
	};
	matrixParams = qs.stringify(matrixParams,';');
	var queryArgs = {
		datefrom:checkIn,
		dateto:checkOut,
		adults:occupancy,
		show_tax_exclusive_rates:"true",
		display_currency:"VND"
	}
	queryArgs= qs.stringify(queryArgs);
	var url = CONFIG.url + '?' + matrixParams + '?' + queryArgs;
	console.log(url);

	request(url,function(err,response,body){
		if (err || response.statusCode != 200) return callback("Check Price Error");
		var $ = cheerio.load(body,{ignoreWhitespace: true, xmlMode: true,lowerCaseTags:true});
		var priceOptions = [];
		$('room').each(function(){
			var priceOption = {};
			priceOption.roomId = $(this).attr('uniqueRoomTypeId');
			var included = "";
			$(this).find('ratePlans').find('inclusions').find('inclusion').each(function(){
				if ($(this).attr("name")) included += $(this).text();
			})	
			priceOption.included = included;
			priceOption.cancellation = {};
			priceOption.cancellation.summary={};
			priceOption.cancellation.summary.en=$(this).find('cancellationPolicy').text();

			var includedNumber = parseInt($(this).find('guestFee').attr('included'));
			var adultRate = parseInt($(this).find('guestFee').attr('adultRate'));

			priceOption.rate = {};
			priceOption.ratePlanId = $(this).find('ratePlan').attr('ratePlanCode');
			priceOption.bookingTarget = $(this).find('ratePlan').attr('bookingAdapterTarget');
			priceOption.rate.daily = [];
			$(this).find('rate').each(function(){
				var extraFee = 0;
				if (occupancy > includedNumber){
					extraFee = adultRate *(occupancy - includedNumber);
				}

				var exclusivePrice = parseInt($(this).find('displayPrice').text()) - extraFee;
				
				var dayRate = {
					date: $(this).attr('day'),
					exclusive: exclusivePrice
				};
				priceOption.rate.daily.push(dayRate);
			})
			priceOption.occupancy = occupancy;
			priceOptions.push(priceOption);
		})
		callback(null,priceOptions);
	})
}

function checkPriceAfterTax(hotelId,occupancy,checkIn,checkOut,priceOptions,callback){
	//var urlString = "http://vws2.ivivu.com:5555/api/search/?param=inventory/dealmatrix/property/"+hotelId + "/rates;apiKey=65af038dd19;lang=vi-VN?";
	var matrixParams = {
		param:"inventory/dealmatrix/property/" + hotelId + "/rates",
		apiKey : CONFIG.key,
		lang:"vi-VN"
	}
	matrixParams = qs.stringify(matrixParams,';');
	var queryArgs = {
		datefrom:checkIn,
		dateto:checkOut,
		adults:occupancy,
		show_tax_exclusive_rates:"false",
		display_currency:"VND"
	}
	queryArgs = qs.stringify(queryArgs);
	var url = CONFIG.url + '?' + matrixParams + '?' + queryArgs;
	console.log(url);

	request(url,function(err,response,body){
		if (err || response.statusCode != 200) return callback("Check Price Error");
		var $ = cheerio.load(body,{ignoreWhitespace: true, xmlMode: true,lowerCaseTags:true});
		var combinePriceOptions = [];
		$('room').each(function(){
			var roomId = $(this).attr('uniqueRoomTypeId');
			var priceOption = _.find(priceOptions,function(correctPriceOption){
				return correctPriceOption.roomId == roomId;
			})

			priceOption.cancellation.summary.vi=$(this).find('cancellationPolicy').text();
			var dayRateList = [];
			
			var includedNumber = parseInt($(this).find('guestFee').attr('included'));
			var adultRate = parseInt($(this).find('guestFee').attr('adultRate'));
		
			$(this).find('rate').each(function(){
				var thisDate = $(this).attr('day');
				var dayRate = _.find(priceOption.rate.daily,function(rate){
					return rate.date == thisDate;
				});

				var inclusive = parseInt($(this).find('displayPrice').text());
				var fee = 0;
				if (occupancy > includedNumber) fee = adultRate * (occupancy - includedNumber);
				var priceWithoutFee = inclusive - fee;
				var tax = priceWithoutFee - dayRate.exclusive;

				dayRate.tax = tax;
				dayRate.fee = fee;
				dayRate.inclusive = inclusive;
				dayRateList.push(dayRate);
			});

			priceOption.rate.daily = dayRateList;
			priceOption.childAge = $(this).find('guestFee').attr('childAgeLimit');
			combinePriceOptions.push(priceOption);
		})
		callback(null,combinePriceOptions);
	})
}


