module.exports = function setup(options, imports, register){
	var availability = require('./availability.js');
	var feeder = require('./feeder.js');
	var pay = require('./pay.js')
	var config = require('./config.js');
	config.url = options.configs.url;
	config.key = options.configs.key;
	config.eta = options.configs.eta;
    config.payment_card_number = options.configs.payment_card_number;
    config.payment_card_holder = options.configs.payment_card_holder;
    config.payment_card_expired_time = options.configs.payment_card_expired_time;
    config.payment_card_code = options.configs.payment_card_code;

	register(null,{
		ivivuService:{
			merchant_id:"ivivu_id",
			availability:availability,
			feeder:feeder,
			pay:pay
		}
	})
}