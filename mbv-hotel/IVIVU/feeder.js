
var request = require('request');
var cheerio = require('cheerio');
var _ = require('lodash');
var async = require('async');
var moment = require('moment');
var qs = require('querystring');
var CONFIG = require('./config.js');
qs.escape = function(s){return s;}


var requestQueue = async.queue(function(options,callback){
	request(options,function(error,response,body){
		if (!error && response.statusCode == 200){
			callback(null,body);
		}else{
			callback("Request + "+ task.url +" failed");
		}
	})
}, 3);

function queuedRequest (options,callback){
	requestQueue.push(options,callback)
}


module.exports = {
	getCountries : function(callback){
		var queryArgs = {
			param: "countries"
		}
		queuedRequest({url:CONFIG.url,qs:queryArgs},function(err,body){
			if (err) return callback("Can't connect to ivivu webservice to get countries. ERROR: " + err);
			parseIvivuCountry(body,function(countries){
				return callback(null,countries)
			})
		})
	},
	getCitiesInCountry : function(country,callback){
		async.waterfall([
			function(next){
				var matrixParams = {
					param: "country/" + country.meta.ivivu_id+"/regions/all/sorted"
				}
				matrixParams =qs.stringify(matrixParams);
				queuedRequest({url:CONFIG.url+'?'+matrixParams},function(err,body){
					if (err) return next ("Can't connect to ivivu web service to get city in country " + country.name.en+ " ERROR: " + err);
					parseIvivuCity(body,function(cities){
						return next(null,cities);
					})
				});
			},
			function(cities,next){
				var filterCities = [];
				async.forEach(cities,function(city,forEachCallback){
					var matrixParams = {
						param: "region/" + city.meta.ivivu_id
					}
					matrixParams = qs.stringify(matrixParams);
					queuedRequest({url:CONFIG.url+'?'+matrixParams},function(err,body){
						if (err) return forEachCallback("Can't connect to ivivu webservice to filter cities for country "+ country.name.en + " ERROR: " + err);
						filterIvivuCity(body,function(filter){
							if (!filter){
								filterCities.push(city);
							}
							forEachCallback();
						})
					})
				},function(err){
					if (err) return next(err);
					return next(null,filterCities);
				})
			}
		],
		function(err,filterCities){
			if (err) return callback(err);
			return callback(null,filterCities);
		});
	},
	getHotelsInCity: function(city,callback){
		async.waterfall([
			function(next){
				var matrixParams = {
					param:"inventory/dealmatrix/properties/region/"+city.meta.ivivu_id,
					apiKey: CONFIG.key
				}
				matrixParams = qs.stringify(matrixParams,';');
				var url = CONFIG.url+'?' + matrixParams
				console.log(url);
				queuedRequest({url:url},function(err,body){
					if (err) return next("Can't connect to ivivu web service to get Hotels for city "+ city.name.en + " ERROR "+ err);
					parseIvivuHotelForHotelId(body,function(hotels){
						return next(null,hotels);
					})
				})
			},
			function(hotels,next){
				var hotelArray = [];
				async.forEach(hotels,function(hotel,callback){
					var matrixParams= {
						param:"inventory/dealmatrix/property/" + hotel.meta.ivivu_id+"/rates",
						apiKey: CONFIG.key
					};
					matrixParams = qs.stringify(matrixParams,';');
					var queryArgs = {
						datefrom: moment().format('YYYY-MM-DD')
					}
					queuedRequest({url:CONFIG.url+'?' + matrixParams+'?'+qs.stringify(queryArgs)},function(err,body){
					//requestQueue.push({url:url},function(err,body){
						if (err) return callback("Cant get Ivivu Hotels Info for city " + city.name.en + " ERROR: " + err);
						parseIvivuHotelForDetail(body,function(returnHotel){
							_.merge(hotel,returnHotel);
							hotel.city_id = city._id;
							hotelArray.push(hotel);
							callback();
						})
					})
				},function(err){
					if (err) return next(err);
					return next(null,hotelArray);
				})
			},
			function(hotels,next){
				var hotelArray = [];
				async.forEach(hotels,function(hotel,callback){
					var matrixParams= {
						param:"inventory/dealmatrix/property/" + hotel.meta.ivivu_id+"/rates",
						apiKey: CONFIG.key,
						lang: "vi-VN"
					};
					matrixParams = qs.stringify(matrixParams,';');
					var queryArgs = {
						datefrom: moment().format('YYYY-MM-DD')
					}
					
					queuedRequest({url:CONFIG.url+'?'+matrixParams+'?'+qs.stringify(queryArgs)},function(err,body){
						if (err) return callback("Can't get Ivivu Hotels Infor for city " + city.name.en + " ERROR: " + err);
						parseIvivuHotelForVNDetail(body,function(returnHotel){
							_.merge(hotel,returnHotel);
							hotelArray.push(hotel);
							callback();
						})
					})
				},function(err){
					if (err) return next(err);
					return next(null,hotelArray);
				})
			}
		],
		function(err,hotelArray){
			if (err) return callback(err);
			console.log(JSON.stringify(hotelArray));
			return callback(null,hotelArray);
		});
	
	}
}

/********************** XML Parse function ************************/
function parseIvivuCountry(xmlBody,callback){
	var $ = cheerio.load(xmlBody,{ignoreWhitespace: true, xmlMode: true});
	var countryArray = {};
	$('country').each(function(i,item){
		countryArray[$(item).attr('isoCode')] = {
			_id:$(item).attr('isoCode'),
			meta:{
				ivivu_id:$(item).attr('isoCode')
			},
			name:{
				en: $(item).text()
			}
		};
	})
	callback(countryArray);
}


function parseIvivuCity(xmlBody,callback){
	var $ = cheerio.load(xmlBody,{ignoreWhitespace: true, xmlMode: true});
	var cityArray = [];
	$('region').each(function(i,item){
		if ($(item).attr('id') == undefined){
			return true;
		}
		cityArray.push({
			meta:{
				ivivu_id:$(item).attr('id')
			},
			name:{
				en:$(item).attr('name')
			}
		});
	});
	callback(cityArray);
}

function filterIvivuCity (xmlBody,callback){
	var $ = cheerio.load(xmlBody,{ignoreWhitespace: true, xmlMode: true});
	var isSub = $('ns2\\:region').attr('parentId') != undefined? true:false
	callback(isSub);
}

function parseIvivuHotelForHotelId (xmlBody,callback){
	var $ = cheerio.load(xmlBody,{ignoreWhitespace: true, xmlMode: true,lowerCaseTags:true});
	var hotelArray = [];
	$('simpleProperty').each(function(){
		hotelArray.push({name:{en:$(this).find('name').text()},meta:{ivivu_id:$(this).attr('id')}});
	});
	return callback(hotelArray);
}

function parseIvivuHotelForDetail(xmlBody,callback){
	var $ = cheerio.load(xmlBody,{ignoreWhitespace: true, xmlMode: true,lowerCaseTags:true});
	var hotel = {};
	hotel.address = {};
	hotel.overview = {};
	hotel.meta={};
	hotel.location = {};
	hotel.rooms = [];
	hotel.facilities = {};
	hotel.facilities.hotel = {};
	hotel.facilities.hotel.en = [];

	var address = '';
	$('address').children().each(function(i,item){
		address = address +  $(this).text() + " ";
	})
	hotel.address.en = address;
	$('facility').each(function(){
		hotel.facilities.hotel.en.push($(this).text());
	})
	hotel.pictures = [];
	$('mediaItem').each(function(){
		hotel.pictures.push($(this).attr('url'));
	})

	hotel.overview.en = $('description').text();
	//hotel.meta.rating = $('rating').attr('value');
	hotel.star= $('rating').attr('value');
	hotel.location.latitude = $('geoCoordinate').attr('latitude');
	hotel.location.longitude = $('geoCoordinate').attr('longitude');
	$('room').each(function(){
		var roomItem = {};
		roomItem.meta = {};
		roomItem.name = {};
		roomItem.name.en = $(this).find('ratePlan').find('name').text();
		roomItem.meta.ivivu_id = $(this).attr('uniqueRoomTypeId');
		var max_occupancy = $(this).find('ratePlans').find('ratePlan').find('guestFee').attr('included');
		var max_adult = $(this).find('ratePlans').find('ratePlan').find('guestFee').attr('maxAdults');
		var max_extrabeds = max_adult - max_occupancy;
		roomItem.max_occupancy = max_occupancy;
		roomItem.max_extrabeds = max_extrabeds;
		 	//console.log(roomItem);
		hotel.rooms.push(roomItem);
		 
	})
		//console.log(JSON.stringify(hotel));
	callback(hotel);
}

function parseIvivuHotelForVNDetail (xmlBody,callback){
	var $ = cheerio.load(xmlBody,{ignoreWhitespace: true, xmlMode: true,lowerCaseTags:true});
	var hotel = {};
	hotel.facilities = {};
	hotel.facilities.hotel={};
	hotel.facilities.hotel.vi=[];
	$('facility').each(function(){
		hotel.facilities.hotel.vi.push($(this).text());
	})
	callback(hotel);
}


