module.exports = function setup(options, imports, register){
	var extract = require('./extract.js');
	var eventBus = imports.eventBus;

	extract.init(eventBus);
	register(null,null);
}
