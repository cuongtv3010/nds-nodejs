var async = require('async');
var Country = require('../Model/Country.js');
var City = require('../Model/City.js');
var Hotel = require('../Model/Hotel.js');

var _ = require('lodash');
var eventBus;
var executor;

exports.init = function(EVENTBUS){
	eventBus = EVENTBUS;

	eventBus.on('info.feed.response',function(payload){
		executor(payload);
	})
}




function extract_cycle (callback){
	async.waterfall([
		function(next){
			//Return most outdated country
			checkCountries(function(err,country){
				return next(err,country);
			});
		},
		function(country,next){
			checkCitiesInCountry(country,function(err,cities){
				if (err) return next(err,country);
				return next(null,country,cities);
			})
		},
		function(country,cities,next){
			async.forEachSeries(cities,function(city,callback){
				checkHotelsInCity(city,function(err,number_hotels){
					if (err) return callback(err);
					city.number_hotels = number_hotels;
					city.meta.updated = new Date();
					city.save(function(err){
						callback();
					})
				})
			},function(err){
				return next(err,country);
			});
		}
	],
	function(err,country){
		console.log(JSON.stringify(country));
		country.meta.updated = new Date();
		country.save(function(saveErr){
			callback(err);
		});
	})
}

//Main function
//exports.run = function (){
function run (){
	async.forever(
		function(next){
			extract_cycle(function(err){
				if (err) console.log(err);
				return next();
			})
		},
		function(err){
		}
	)
}

setTimeout(function(){
	run();
},6000)



/*************************Check Countries In Both Ivivu And Agoda Functions **************************/
function checkCountries(callback){
	console.log("Check Countries ");
	async.waterfall([
		function(next){
			var payload = {type:'getCountries'};
			eventBus.emit('info.feed.request',payload);
			executor = function(payload){
				if (payload.error) return next(payload.error);
				return next(null,payload.result);
			}
		},
		function(countries,next){
			async.forEach(_.values(countries),function(country,callback){
				countryHandler(country,function(err){
					callback(err);
				})
			},function(err){
				return next(err);
			});
		},
		function(next){
			Country.findMostOutdatedCountry(function(err,country){
				if (err) return next(err);
				return next(null,country);
			})
		}
	],function(err,country){
		if (err) return callback(err);
		return callback(null,country);
		
	})
}

function countryHandler(country,callback){
	Country.findById(country._id,function(err,returnCountry){
		if (err) return callback(err);
		if (! returnCountry){
			(new Country(country)).save(function(err){
				return callback(err);
			});
		}else{
			_.merge(returnCountry.meta,country.meta);
			_.merge(returnCountry.name,country.name);
			returnCountry.save(function(err){
				return callback(err);
			});
		}
	})
}


/**********************Check Cities In Selected Country In Both Agoda And Ivivu Functions *************/
/*
	country follows Country Model in ./Model/Country.js
 */

function checkCitiesInCountry (country,callback){
	console.log("Check cities for country " + country.name.en);

	var citiesHandler = function(cities,handlerCallback){
		async.eachSeries(cities,function(city,callback){
			city.country_id = country._id;
			var keywords = _.unique([city.name.en.toLowerCase(),city.name.vi != undefined ? city.name.vi.toLowerCase():undefined,country.name.en.toLowerCase(),country.name.vi!=undefined? country.name.vi.toLowerCase(): undefined]);
			keywords = keywords.filter(function(n) { return n != undefined});
			city.meta.keywords = keywords;
			cityHandler(city,function(err){
				callback(err);
			})
		},function(err){
			return handlerCallback(err);
		})
	}

	var payload = {type:'getCities',argument:country};
	eventBus.emit('info.feed.request',payload);	

	executor = function(payload){
		if (payload.error) return callback(payload.error);
		var cities = payload.result;
		citiesHandler(cities,function(err){
			if(err) return callback(err);
			City.find({"country_id":country._id},function(err,cities){
				if (err) return callback(err);
				return callback(null,cities);
			})
		})
	}		
}

function cityHandler(city,callback){
	async.waterfall([
		function(next){
			var id = city.merchant_id;
			delete city.merchant_id;

			var keyword = 'meta.' + id ;
			var merchant_id = city.meta[id];

			var where = {};
			where[keyword] = merchant_id;
			City.findOne(where,function(err,databaseCity){
				if (err) return next(err);
				return next(null,databaseCity);
			})
		},
		function(databaseCity,next){
			if (databaseCity) return next(null,databaseCity);
			City.findOne({"name.en":city.name.en},function(err,returnCity){
				if (err) return next(err);
				return next(null,returnCity);
			})
		},
		function(databaseCity,next){
			if (!databaseCity){
				
				(new City(city)).save(function(err){
					return next(err);
				})
			}else{
				_.merge(databaseCity.meta,city.meta);
				_.merge(databaseCity.name,city.name);
				databaseCity.save(function(err){
					return next(err);
				})
			}
		}
	],function(err){
		return callback(err);
	});
}

/**Check hotels in selected City in both Agoda and Ivivu **/
function checkHotelsInCity(city,callback){
	console.log("Check Hotels in city " + city.name.en);
	var number_hotels = 0; 
	var hotelsHandler = function(hotels,handlerCallback){
		async.eachLimit(hotels,5,function(hotel,callback){
			hotel.city_id = city._id;
			var keywords = (hotel.name.en || '') + ' ' + (hotel.name.vi || '');
			keywords = keywords.toLowerCase().split(' ');
			keywords = _.unique(keywords);
			keywords = _.filter(keywords, function(kw){ return kw && (kw.length >2)});
			hotel.meta.keywords = keywords;
			hotelHandler(hotel,function(err){
				callback(err);
			})
		},function(err){
			return handlerCallback(err);
		})
	}


	var payload = {type:'getHotels',argument:city};
	eventBus.emit('info.feed.request',payload);	
	executor = function(payload){
		if (payload.error) return callback(payload.error);
		var hotels = payload.result;
		hotelsHandler(hotels,function(err){
			console.log("Done checking hotels in city " + city.name.en);
			if (err) return callback(err);
			return callback(null,hotels.length);
		})

	}		
}

function hotelHandler(hotel,callback){
	var id; 
	async.waterfall([
		function(next){
			id = hotel.merchant_id;
			delete hotel.merchant_id;

			var keyword = 'meta.'+ id
			var merchant_id = hotel.meta[id];
			var where = {};
			where[keyword] = merchant_id;
			Hotel.findOne(where,function(err,databaseHotel){
				if (err) return next(err);
				return next(null,databaseHotel);
			})
		},
		function(databaseHotel,next){
			if (!databaseHotel){
				(new Hotel(hotel)).save(function(err){
					return next(err);
				})
			}else{
				 _.merge(databaseHotel.meta,hotel.meta);
				 _.merge(databaseHotel.name,hotel.name);
				 databaseHotel.star = hotel.star,
				 _.merge(databaseHotel.facilities,hotel.facilities);
				 _.merge(databaseHotel.location,hotel.location);
				 databaseHotel.rates_from = hotel.rates_from;
				 _.merge(databaseHotel.address,hotel.address);
				 _.merge(databaseHotel.overview,hotel.overview);
				 _.merge(databaseHotel.pictures,hotel.pictures);

				//Deal with hotel rooms 
				 _(databaseHotel.rooms).each(function(databaseRoom){
				 	var mergeRoom = _.find(hotel.rooms,function(hotelRoom){
				 		return hotelRoom.meta[id] == databaseRoom.meta[id];
				 	});
				 	//Room doesn't appear on the latest hotel check, do remove
				 	if (mergeRoom){
				 		_.merge(databaseRoom,mergeRoom);
				 		var roomIndex = _.findIndex(hotel.rooms,function(hotelRoom){
				 			return hotelRoom.meta[id] == mergeRoom.meta[id];
				 		})
				 		hotel.rooms.splice(roomIndex,1);
				 	}	
				 })
				 //After remove , check if any room left in hotelArray ===> new Room
				 _(hotel.rooms).each(function(hotelRoom){
				 	databaseHotel.rooms.push(hotelRoom);
				 })

				 databaseHotel.save(function(err){
				 	return next(err);
				 })
			}
		}
	],function(err){
		return callback(err);
	});
}







