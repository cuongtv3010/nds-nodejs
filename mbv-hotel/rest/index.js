module.exports = function setup(options, imports, register) {
    var restify = require('restify');
    var hotelController = require('./hotelController');
    

    var server = restify.createServer();
    server.use(function(req, res, next) {
        res.header('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Expose-Headers', 'Location');
        return next();
    });

    server.opts('.*', function(req, res, next) {
        res.header('Access-Control-Allow-Credentials', true);
        res.header('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type,Accept,Origin,X-Requested-With,Content-Type,Accept,Origin');
        res.header('Access-Control-Allow-Methods', 'GET,POST,HEAD,GET,POST,HEAD');
        res.send(200);
    });

    server.use(restify.queryParser());
    server.use(restify.bodyParser());

    hotelController(imports.hotelService, function(err, controller){
        server.get('/HotelService/country', controller.GetCountries);
        server.get('/HotelService/city', controller.SearchCityByKeywords);
        server.get('/HotelService/city/:id', controller.SearchCityById);
        server.get('/HotelService/hotel', controller.SearchHotelByKeywords);
        server.get('/HotelService/hotel/:id', controller.SearchHotelById);
        server.get('/HotelService/rate', controller.SearchRate);
        server.get('/HotelService/rate/:id', controller.GetRate);
        server.post('/HotelService/book', controller.CreateBooking);
        server.get('/HotelService/book/:id', controller.GetBooking);    
        server.get('/HotelService/book/:id/status', controller.GetBookingStatus);    
        server.post('/HotelService/pay', controller.Pay);    
    });
    //HOTELS
    
    //Pay

    server.listen(options.port, function() {
        //server.listen(process.env.PORT || 8080, function() {
        console.log('%s listening at %s', server.name, server.url);
        register(null, null);
    });
    
}
