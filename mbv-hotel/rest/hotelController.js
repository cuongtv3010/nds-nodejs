module.exports = function init(service, callback){
    
    var hotelService = service;
    var moment = require('moment');
    var Validator = require('jsonschema').Validator;
    var requestValidator = new Validator();
    var bookRequestSchema = {
	"type": "object",
	"properties": {
		"agent" : {
			"type": "object",
    		"properties": {
				"id": { "type": "string", "minLength": 1, "required": true },        		
        		"mobile":{ "type": "string", "minLength": 1, "required": true },
        		"email": { "type": "string", "minLength": 1, "required": true }
			},
			"additionalProperties": false,
			"required": true
		},
      	"guests": {
      		"type": "array", 
      		"items": {
      			"type": "object", 
				"properties" : {
					"firstName": { "type": "string", "minLength": 1, "required": true, "pattern": "^[a-zA-Z- ]+$" },
					"lastName": { "type": "string", "minLength": 1, "required": true, "pattern": "^[a-zA-Z- ]+$" },
					"email": { "type": "string", "minLength": 1, "required": true, "pattern": "^[A-Za-z0-9-.\_]+\@[A-Za-z0-9-.\_]+\.[A-Za-z]{2,4}$" },
					"mobile": { "type": "string", "minLength": 1, "required": true, "pattern": "^[\+]?[0-9 \-]+$" },
					"nationality": { "type": "string", "minLength": 1, "required": true }	
				},
				"additionalProperties": false
      		}, 
      		'minItems': 1, 
      		"required": true
      	},
      	"book" : {
      		"type": "object",
      		"properties": {
				"hotelRateId": { "type": "string", "minLength": 1, "required": true },        		
        		"rooms" : {
        			"type": "array", 
      				"items": {
      					"type": "object", 
						"properties" : {
							"roomRateId": { "type": "string", "minLength": 1, "required": true },
							"quantity": { "type": "integer", "minimum": 1, "required": true }								
						},
						"additionalProperties": false
      				}, 
      				'minItems': 1,
      				"required": true
        		}
			},
			"additionalProperties": false,
			"required": true
      	}     
    },
    "additionalProperties": false
    }
    var controller = {
        GetCountries: function(req, res, next){
            hotelService.getCountryList(function(err, countries){
                if(err){
                    res.send(500);
                }else{
                    res.send(200, countries);
                }
                return next();
            });
        },
        
        SearchCityByKeywords: function(req, res, next){
            var searchStr = req.query.str || '';
            if(searchStr.length < 3){
                res.send(400);
                return next();
            }
            hotelService.searchCityByKeywords(searchStr, 0, 20, function(err, cities){
                if(err){
                    res.send(500);
                    return next();
                }
				res.send(200, cities);
                return next();
			});	
        },
        
        SearchCityById: function(req, res, next){
            var id = req.params.id;
            if(!id){
                res.send(400);
                return next();
            }
            hotelService.searchCityById(id, function(err, city){
                if(err){
                    res.send(500);
                    return next();
                }
				res.send(200, city);
                return next();
            });
        },
        
        SearchHotelByKeywords: function(req, res, next){
            var cityId = req.query.city;
            if(!cityId){
                res.send(400);
                return next();
            }
            var keyword = req.query.kw;
            var skip = 0;
            var limit = 10;
            if(req.query.page){
                skip = (req.query.page - 1) * limit;
                if(skip < 0) skip = 0;
            } 
            var sort = req.query.sort || 'rating';
            
            hotelService.searchHotelByKeywords(cityId, keyword, skip, limit, sort, function(err, count, hotels){
                 if(err){
                    res.send(500);
                    return next();
                }
				res.send(200, {total: count, hotels: hotels});
                return next();
            });
        },
        
        SearchHotelById: function(req, res, next){
            var id = req.params.id;
            if(!id){
                res.send(400);
                return next();
            }
            hotelService.searchHotelById(id, function(err, hotel){
                if(err){
                    res.send(500);
                    return next();
                }
				res.send(200, hotel);
                return next();
            });
        },
        
        SearchRate: function(req, res, next) {
	        if(!req.query.hotelId){
		        res.send(400);
		        return next();
	        }
	        if(!req.query.checkIn || moment(req.query.checkIn + ' 23:59:59', "YYYY-MM-DD HH:mm:ss").isBefore(moment())){
		        res.send(400);
		        return next();
	        }
	        if(!req.query.checkOut || moment(req.query.checkOut, "YYYY-MM-DD").isBefore(moment(req.query.checkIn, "YYYY-MM-DD"))){
		        res.send(400);
		        return next();
            }
            hotelService.checkRate(req.query.hotelId, req.query.checkIn, req.query.checkOut, function(err, id){
                if(err){
				    console.log(err);
				    res.send(400);
				    return next();
                }
                res.setHeader('Location', '/HotelService/rate/' + id);
                res.send(202);
                return next();
	        });
        },
        
        GetRate: function(req, res, next) {
            var id = req.params.id;
             if(!id){
                res.send(400);
                return next();
            }
            hotelService.getRate(id, function(err, rate){
                if(!rate){
                    res.send(204);
                    return next();
                }
				res.send(200, rate);
                return next();
            });
        },
        
        CreateBooking: function(req, res, next) {
            var command = req.params || {};
			var validation = requestValidator.validate(command, bookRequestSchema);
			if(validation.errors.length > 0){
				console.log(validation);
				res.send(400);
				return next('Invalid Request');
			}	
			hotelService.createBooking(command, function(err, bookingId){
			    if(err){
			        console.log(err);
				    res.send(400);
				    return next();
			    }
                console.log(bookingId);
			    res.setHeader('Location', '/HotelService/book/' + bookingId);
                res.send(202);
                return next();
			})
        },
        
        Pay: function(req, res, next){
            var id = req.params.bookId;
            if(!id){
                res.send(400);
                return next();
            }
            console.log('Pay ' + id);
            hotelService.payBooking(id, function(err){
               if(err){
                   console.log(err);
                    res.send(400);
                    return next();
               } 
                res.send(202);
                return next();
            });
        },
        
        GetBooking: function(req, res, next) {
            var id = req.params.id;
            if(!id){
                res.send(200);
                return next();
            }
            hotelService.getBooking(id, function(err, booking){
                if(err){
			        console.log(err);
				    res.send(400);
				    return next();
			    }
                res.send(200, booking);
                return next();
			})
        },
        
        GetBookingStatus: function(req, res, next) {
            var id = req.params.id;
            if(!id){
                res.send(200);
                return next();
            }
            hotelService.getBooking(id, function(err, booking){
                if(err){
			        console.log(err);
				    res.send(400);
				    return next();
			    }
                res.send(200, {id: booking.id, status: booking.status});
                return next();
			})
        },
    };
    callback(null, controller);
};
