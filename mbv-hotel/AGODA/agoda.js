module.exports = function setup(options, imports, register){
	var availability = require('./availability.js');
	var feeder = require('./feeder.js');
	var pay = require('./pay.js');
	
	var config = require('./config.js');
	
	config.lang_vn=options.configs.lang_vn;
	config.currency = options.configs.currency;
	config.siteId = options.configs.siteId;
	config.url = options.configs.url;
	config.key = options.configs.key;

    config.payment_card_number1 = options.configs.payment_card_number_1;
    config.payment_card_number2 = options.configs.payment_card_number_2;
    config.payment_card_number3 = options.configs.payment_card_number_3;
    config.payment_card_number4 = options.configs.payment_card_number_4;
    config.payment_card_holder = options.configs.payment_card_holder;
    config.payment_card_expired_month = options.configs.payment_card_expired_month;
    config.payment_card_expired_year = options.configs.payment_card_expired_year;
    config.payment_card_code = options.configs.payment_card_code;
    config.payment_card_bank = options.configs.payment_card_bank;
    register(null,{
		agodaService:{
			merchant_id:"agoda_id",
			availability:availability,
			feeder: feeder,
			pay:pay
		}
	})


	
}