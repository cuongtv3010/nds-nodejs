var spawn = require('child_process').spawn;var cheerio = require('cheerio');
var _ = require('lodash');
var moment = require('moment');
var async = require('async');
var url = require('url');
var zlib = require('zlib');
var wd = require('wd');
var asserters = wd.asserters;
var qs = require('querystring');
var CONFIG  = require('./config.js');



function paymentSubmit(command,browser,callback){
    async.waterfall([
        function(next){
            var checkIn = moment(command.checkIn);
            var checkOut = moment(command.checkOut);
            var nights = checkOut.diff(checkIn, 'days');

            var url = 'http://www.agoda.vn/partners/SearchBox/SearchResult.aspx';
            var args = {
                    Room: 1,
                    Adult: 2,
                    Children: 0,
                    Search: command.cityName,
                    Night: nights,
                    ArrivalDate: checkIn.format("MMMM/DD/YYYY"),
                    DepartDate: checkOut.format("MMMM/DD/YYYY"),
                    LanguageID: 1,
                    HotelID: command.hotelId,
                    Header: '',
                    Footer: '',
                    CID: CONFIG.siteId,
                    CurrencyCode: CONFIG.currency,
                    radius: 36,
                    cityid: 0
                };

            args = qs.stringify(args);
            url = url + "?" + args;
            console.log(url);

            browser.get(url,function(err){
                if (err) {return next(err);}
                else{ 
                    browser.setWindowSize(1440,900,'current',function(err){
                        return next();    
                    })
                }
                
            });
        },
        function (next){
            browser.elementById("ctl00_ctl00_MainContent_ContentMain_RoomTypesListGrid_AB1771_lbMoreRoomPlus",function(err,el){
                if (err) return next();
                el.click(function(err){
                    return next();
                })
            });
        },

        function(next){
            browser.source(function(err,html){
                var content = cheerio.load(html);

                var roomUrl;
                var roomPricesBox = content('div#ctl00_ctl00_MainContent_ContentMain_RoomTypesListGrid_AB1771_upRoomTypes');
                var found;

                _(_.range(100)).forEach(function(ind) {
                    var roomId = 'a#ctl00_ctl00_MainContent_ContentMain_RoomTypesListGrid_AB1771_rptRateContent_ctl' + (ind < 10 ? '0' : '') + ind + '_lblRoomNameLink';

                    roomId = content(roomId, roomPricesBox);


                    if (roomId.length != 1) return false;

                    roomId = url.parse(roomId.attr('href'), true).query.rmtID;
                    if (roomId != command.roomId) return true;

                    // Number of occupancy
                    var occupancy = content('a#button_room' + ind, roomPricesBox);

                    if (occupancy.length != 1) return false;

                    var preBookLink = occupancy.attr('href');

                    occupancy = parseInt(occupancy.attr('occupancy'), 10);

                    if (occupancy != command.occupancy) return true;

                    // Extra beds
                    var extrabeds = content('a#lnkExtrabed' + ind, roomPricesBox).length !== 0;

                    if (command.extrabeds != 0 && extrabeds == false) return true;

                    // Promotion or Flash sales
                    var promo = 'td#ctl00_ctl00_MainContent_ContentMain_RoomTypesListGrid_AB1771_rptRateContent_ctl' + (ind < 10 ? '0' : '') + ind + '_tdRoom1 table.room_pro';

                    promo = content(promo, roomPricesBox).length == 1;


                    var flash = 'td#ctl00_ctl00_MainContent_ContentMain_RoomTypesListGrid_AB1771_rptRateContent_ctl' + (ind < 10 ? '0' : '') + ind + '_tdRoom1 table.room_flash';

                    flash = content(flash, roomPricesBox).length == 1;

                    if ((promo == true || flash == true) && !command.promoId) return true;

                    if ((promo == false && flash == false) && command.promoId) return true;

                    found = ind;
                    return false;
                });
                if (found !== undefined) return next(null,found);
                return next('Room Not Found');
            });
        },

        function(index,next){
            console.log("Chon so luong phong");
            browser.elementById("room"+index,function(err,el){
                el.click(function(){
                    browser.elementByXPath("//select[@id='room"+index+"']//option[@value='"+command.quantity+"']",function(err,el){
                        el.click(function(){
                            return next(null,index);
                        })
                    });
                })
            });
        },

        function(index,next){
            console.log("Click button");
            browser.elementById("button_room"+index,function(err,el){
                el.click(function(){
                    browser.waitForElementById("txtCntFirstName",25000,function(err,el){
                        if (err) return next("Can't load page, the AGODA payment interface may have changed");
                        return next();
                    })
                    //return next(null);
                });
            });
        },

        /********************Submit room page ************************/
        function (next){
            console.log("Send First Name");

            browser.elementById("txtCntFirstName",function(err,el){
                if (err) return next(err);
                el.clear(function(){
                    el.sendKeys(command.guests[0].firstName,function(){
                        console.log("Send first Name success");
                        return next();
                    });
                });

            });            
        },

        function (next){
            console.log("Send Last Name");

            browser.elementById("txtCntLastName",function(err,el){
                if (err) return next(err);
                el.clear(function(){
                    el.sendKeys(command.guests[0].lastName,function(){
                        console.log("Send last name success");
                        return next();
                    });
                });

            });
        },
        function (next){
            console.log("Send Email");
            browser.elementById("txtEmail",function(err,el){
                if (err) return next(err);
                el.clear(function(){
                    el.sendKeys(command.guests[0].email,function(){
                        console.log("Send Email success");
                        return next();
                    });
                });

            });
            
        },

        function (next){
            console.log("Send Confirmation Email");
            browser.elementById("txtConfirmEmail",function(err,el){
                if (err) return next(err);
                el.clear(function(){
                    el.sendKeys(command.guests[0].email,function(){
                        console.log("Send Successfull email success");
                        return next();
                    });
                });

            });
            
        },
        function (next){
            console.log("Send Phone No");
            browser.elementById("txtPhoneNo",function(err,el){
                if (err) return next(err);
                el.clear(function(){
                    el.sendKeys(command.guests[0].mobile,function(){
                        console.log("Send Phone No Success");
                        return next();
                    });
                });

            });
            
        },
        function (next){
            console.log("Send national Id");
            browser.elementById("ddlCntctCountry",function(err,el){
                if (err) return next(err);
                el.click(function(){
                    browser.elementByXPath("//select[@id='ddlCntctCountry']//option[@value='"+command.guests[0].nationalId+"']",function(err,el){
                        if (err) return next("Incorrect nationality");
                        el.click(function(){
                            return next();
                        });

                    })
                })
            })
        },
        function(next){
            console.log("Check extra bed");
            if (command.extrabeds === 0) return next();
            browser.elementById('bkg_llblExtrabedOccFree',function(err,el){
                if (err) return next("This room doesn't have extra beds");
                el.click(function(){
                    async.forEachSeries(_.range(command.extrabeds),function(index,callback){
                        browser.elementById('ContentPlaceHolder1_chklstExtraBed_'+index,function(err,el){
                            if (err) return callback("extrabeds number doesn't match"); 
                            el.click(function(){
                                return callback();
                            });
                        })
                    },function(err){
                        return next(err);
                    })
                });
            });
        },
        function(next){
            console.log("Click Tiep Tuc Button");
            browser.elementByCssSelector('input#btnContinue',function(err,el){
                el.click(function(){
                    console.log("Click successfully");
                    browser.waitFor(asserters.jsCondition('$("#divstep2").is(":visible") ? true : false'), 20000, function(err){
                        if (err) return next(err);
                        return next();
                    }); 
                })
            })
        },
        function(next){
            console.log("Check gia");
            browser.source(function(err,html){
                var $ = cheerio.load(html);
                var finalPrice = $('div#pnlTotalPrice table tbody tr td.bkgdt_col3 span').text();
                finalPrice = parseInt(finalPrice.split(".").join(""));
                console.log("Final Price for " + command.bookingId + " is " + finalPrice);
                if (Math.abs(finalPrice - command.total) >= 100) return next('Invalid Command: Final Price Not Match ' + finalPrice);
                return next();
            })
        },
        function(next){
            console.log("Phuong thuc thanh toan");
            browser.elementById('ddlPaymentMethod',function(err,el){
                if(err) return next(err);
                el.click(function(){
                    browser.elementByXPath("//select[@id='ddlPaymentMethod']//option[@value='1']",function(err,el){
                        el.click(function(){
                            return next();
                        });

                    })
                })
            });
            
        },
            function(next){
                browser.elementById("txtCvc",function(err,el){
                    if (err) return next(err);
                    el.clear(function(){
                        el.sendKeys(CONFIG.payment_card_code,function(){
                            return next();
                        });
                    });
                });
            },
        function(next){
            browser.execute('$("#txtCard_0").val("'+CONFIG.payment_card_number1+'")',function(err){
                if (err) return next(err);
                return next();
            });
        },
        function(next){
            browser.execute('$("#txtCard_1").val("'+CONFIG.payment_card_number2+'")',function(err){
                if (err) return next(err);
                return next();
            });
        },
        function(next){
            browser.execute('$("#txtCard_2").val("'+CONFIG.payment_card_number3+'")',function(err){
                if (err) return next(err);
                return next();
            });
        },
        function(next){
            browser.execute('$("#txtCard_3").val("'+CONFIG.payment_card_number4+'")',function(err){
                if (err) return next(err);
                return next();
            });
        },
        function(next){
            browser.elementById("txtHolderName",function(err,el){
                if (err) return next(err);
                el.clear(function(){
                    el.sendKeys(CONFIG.payment_card_holder,function(){
                        return next();
                    });
                });
            });
        },

        function(next){
            browser.elementById('ddlExpiryMonth',function(err,el){
                if(err) return next(err);
                el.click(function(){
                    browser.elementByXPath("//select[@id='ddlExpiryMonth']//option[@value='"+CONFIG.payment_card_expired_month+"']",function(err,el){
                        el.click(function(){
                            return next();
                        });

                    })
                })
            });
        },
        function(next){
            browser.elementById('ddlExpiryYear',function(err,el){
                if(err) return next(err);
                el.click(function(){
                    browser.elementByXPath("//select[@id='ddlExpiryYear']//option[@value='"+CONFIG.payment_card_expired_year+"']",function(err,el){
                        el.click(function(){
                            return next();
                        });

                    })
                })
            });
        },
        function(next){
            browser.elementById('ddlIssueBankCountry',function(err,el){
                if(err) return next(err,[]);
                el.click(function(){
                    browser.elementByXPath("//select[@id='ddlIssueBankCountry']//option[@value='38']",function(err,el){
                        el.click(function(){
                            return next();
                        });

                    })
                })
            });
        },
        function(next){
            browser.elementById("txtIssueBank",function(err,el){
                if (err) return next(err);
                el.clear(function(){
                    el.sendKeys(CONFIG.payment_card_bank,function(){
                        return next();
                    });
                });
            }); 
        },
        function(next){
            browser.elementById('btnSubmit',function(err,el){
                if (err) return next(err);
                el.click(function(){
                    return next();
                })
            })
        },
        function(next){
            browser.source(function(err,html){
                var $ = cheerio.load(html);
                var reservationCode = $('div.msgconfirm strong').first().text();
                if (reservationCode){
                    return next(null,{bookingId: command.bookingId, code:"BUY_SUCCESS",message:reservationCode});
                }else{
                    return next(null,{bookingId: command.bookingId, code:"BUY_UNKNOWN",message:"Unable to get reservation code"});
                }
            })
        }
    ],
    function (err,result){
        if (err) {
            callback(err,{bookingId: command.bookingId,code:"BUY_ERROR",message: err.toString()});
        }else{
            callback(null, result);
        }
    }
    );
}

function createBrowser (port,callback){
    var child = spawn('phantomjs', ['--webdriver=' + port]);
  //  var child = spawn('chromedriver', ['--url-base=/wd/hub', '--port=' + port]);
    var browser;
    child.stdout.on('data', function (data) {
        console.log('stdout: ' + data);
        if (data.toString().indexOf('running') > -1) {
            if(browser === undefined)
            {
                browser = wd.remote("localhost", port);
                browser.init({browserName: 'chrome', "phantomjs.page.settings.userAgent": 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36',"phantomjs.page.settings.loadImages":false}, function(err){
                    if (err) callback(err);
                    callback(null, browser,child);
                    
                }); 
            }
        }
    });

    child.on('success', function (err) {
      console.log('ls error', err);
    });


    child.stderr.on('data', function (data) {
      console.log('stderr: ' + data);
    });

    child.on('close', function (code) {
      console.log('child process exited with code ' + code);
    });
}

var portPool = [12000,12001,12002,12003];

var phantomQueue =  async.queue(function(command,phantomCallback){
    var port = portPool.splice(0,1);
    createBrowser(port, function(err,browser,child){
        if (err) return phantomCallback(err);
        paymentSubmit(command,browser,function(errResult,result){
            browser.quit(function(){
                child.kill();
                portPool.push(port[0]);
                console.log(portPool);
                return phantomCallback(errResult,result);
            })
        })
    })
},4);

exports.handle = function(command,callback){
    console.log("Received command " + JSON.stringify(command));

    phantomQueue.push(command,function(err,result){
        console.log(err);
        console.log(result);
        if (err){
            return callback(err,{
                bookingId: result.bookingId,
                code: 'BUY_ERROR',
                msg: result.message
            })
        }else{
            return callback(null,{
                bookingId: result.bookingId,
                code: result.code,
                reservationCode: result.message
            })
        }
    })
}
