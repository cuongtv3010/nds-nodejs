module.exports = function setup(options, imports, register){
	var mongoose = require('mongoose');
	var _ = require('lodash');
	var async = require('async');
    

	var Country = require('../Model/Country.js');
	var City = require('../Model/City.js');
	var Hotel = require('../Model/Hotel.js');
    var Booking = require('../Model/HotelBooking.js');
    var eventBus = imports.eventBus;
    
    
    var redis = require("redis");
    var cacheTTL = 30 * 60; //seconds
    var cache = redis.createClient(options.configs.redisPort, options.configs.redisUrl);
    cache.auth(options.configs.redisKey);
    cache.on("connect", function(){console.log('Redis Connected...')});
    cache.on("error", function (err) {
        console.log("error event - " + cache.host + ":" + cache.port + " - " + err);
    })

    var executionMap = {};


    eventBus.on('hotel.avail.response',function(payload){
        var id = payload.result.hotelRateId;
        if (executionMap[id]){
            executionMap[id](payload);
            delete executionMap[id];
        }
    })

    eventBus.on('hotel.pay.response',function(payload){ 
        var id = payload.result.bookingId;
        if (executionMap[id]){
            executionMap[id](payload);
            delete executionMap[id];
        }
    })    

    
	var CityConverter = function(city) {
        return {
            id: city.id, 
            country_id: city.country_id, 
            name: city.name, 
            number_hotels: city.number_hotels
        };
    };

    var HotelConverter = function(hotel){
        return {
            id: hotel.id, 
			cityId: hotel.city_id,
			star: hotel.star,
			ratesFrom: hotel.rates_from,
            pictures: hotel.pictures,
            overview: hotel.overview,
            address: hotel.address,
            location: hotel.location,
            name: hotel.name,
            rooms: _.map(hotel.rooms, 
                function(room) {return {
                    id: room.id, 
                    name: room.name, 
                    max_occupancy: room.max_occupancy, 
                    max_extrabeds: room.max_extrabeds, 
                    picture: room.picture
                };})
        };
    };

	var hotelService = {
		getCountryList : function(callback){
			Country.find({},function(err,countries){
				if (err) return callback(err);
				async.map(countries,
                    function(country, callback) {
                        callback(null, {
                            id: country.id,
                            name: country.name
                        });
                    },
                    function(err, results) {
                        return callback(null, results);
                    }
                );
			})
		},
		searchCityById: function(id, callback) {
            City.findById(id, function(err, city){
                if(err){
                    console.log(err);
                    return callback(null, null);  
                } 
                if(city) city = CityConverter(city);
                return callback(null, city);
            });
        },
        
        searchCityByKeywords: function(keyword, skip, limit, callback) {
            City.search(keyword,skip,limit, function(err, cities){
                if(err){
                    console.log(err);
                    return callback(null, null);  
                } 
                async.map(cities, 
                    function(city, callback){
                        callback(null, CityConverter(city));
                    }, 
                    function(err, result){
                        return callback(err, result);
                    }
                );
            });
        },
        
        searchHotelById: function(id, callback) {
            Hotel.findById(id, function(err, hotel){
                if(err){
                    logger.error(err);
                    return callback(null, null);
                }
                if(hotel) hotel = HotelConverter(hotel);
                return callback(null, hotel);
            });
        },
        
        searchHotelByKeywords: function(cityId, keywords, skip, limit, sort, callback){
            keywords = (keywords||'').toLowerCase();
            keywords = keywords.replace(/[\.,-]/g, " ");
            if(keywords.length > 0){
                keywords = _.unique(keywords.split(' '));
                keywords = _.filter(keywords, function(entry) { return entry.length > 0; });
                if(keywords.length === 0) keywords = null;
            }
            Hotel.search(cityId, keywords, skip, limit, sort, function(err, count, hotels){
                if(err){
                    console.log(err);
                    return callback(err);  
                } 
                async.map(hotels, 
                    function(hotel, callback){
                        callback(null, HotelConverter(hotel));
                    }, 
                    function(err, result){
                        return callback(err, count, result);
                    }
                );
            });
        },
		checkRate : function(hotelId,checkIn,checkOut,callback){
            Hotel.findById(hotelId,function(err,hotel){
				if (err || !hotel) return callback("Hotel not found");
                var id = new mongoose.Types.ObjectId();
                var command = {
					id: id,
					type: 'checkAvailability',
					payload:{
						hotelId: hotel.id,
			            meta: hotel.meta,
			            checkIn: checkIn,
			            checkOut: checkOut,
			            rooms: []
					}
				};

                _(hotel.rooms).forEach(function(room){
                    var commandRoom = {
                        id: room._id,
                        max_occupancy: room.max_occupancy,
                        max_extrabeds: room.max_extrabeds,
                        meta: room.meta
                    }
                    command.payload.rooms.push(commandRoom);
                })

                eventBus.emit('hotel.avail.request',{command:command});

                var handler = function(payload){
                    if (!payload.error){
                        var result = payload.result;
                        console.log("Result is " + JSON.stringify(result));
                        cache.set("HotelRate." + result.hotelRateId,JSON.stringify(result));
                        cache.expire("HotelRate."+result.hotelRateId,cacheTTL);
                    }
                    callback(null,id);
                }
                executionMap[id] = handler;
                _.delay(function(){
                    if (executionMap[id]){
                        console.log("Timeout delete " + id);
                        var payload = {error:"Time out",result: {}}
                        executionMap[id](payload);
                        delete executionMap[id];
                    }
                },150000)
			})
		},

        getRate : function(id,callback){
            cache.get("HotelRate."+id,function(err,cacheValue){
                if (err) return callback(err);
                if (cacheValue){
                    console.log(JSON.parse(cacheValue));
                    return callback(null,JSON.parse(cacheValue));
                }else{
                    return callback("rates not found");
                }
            })
        },
        
        createBooking: function(command, callback) {
            async.waterfall([
                function(next){
                    cache.get('HotelRate.' + command.book.hotelRateId, function (err, reply) {
                        if(err) return next(err);
                        if(!reply) return next('HotelRate Not Found');
                        return next(null, JSON.parse(reply));
                    });
                },
                function(hotelRate, next){
                    var booking = new Booking();
                    booking.hotelId = hotelRate.hotelId;
                    booking.checkIn = hotelRate.checkIn;
                    booking.checkOut = hotelRate.checkOut;
                    booking.agent = command.agent;
                    booking.guests = command.guests;        
                    booking.rooms = [];
                    booking.total = 0;
                    _(command.book.rooms).forEach(function(room){
                        var tmp = _.find(hotelRate.rates, function(rate) {return rate.roomRateId == room.roomRateId;});
                        if(!tmp) return false;
                        booking.rooms.push({
                            quantity: room.quantity,
                            roomId: tmp.roomId,
                            //ratePlanId : tmp.ratePlanId,
                            //bookingTarget : tmp.bookingTarget,
                            occupancy: tmp.occupancy,
                            extrabeds: tmp.extrabeds,
                            rate: tmp.rate,
                            promoId: tmp.promoId,
                            included: tmp.included,
                            cancellation: tmp.cancellation                                          
                        });                     
                        booking.total = booking.total + tmp.rate.total * room.quantity;
                    });
                    
                    if(booking.rooms.length != command.book.rooms.length){
                        return next('Invalid Room Rate');
                    }
                    booking.save(next); 
                }],
                function(err, booking){
                    console.log(booking);
                    if(err) return callback(err);
                    if(booking) return callback(null, booking.id);
                    return callback(null, null);
                }
            );
        },

        getBooking: function(id, callback) {
            Booking.findById(id, function(err, booking){
                if(err) return callback(err);
                return callback(null, {
                    id: booking.id,
                    hotelId: booking.hotelId,
                    checkIn: booking.checkIn,
                    checkOut: booking.checkOut,
                    rooms: booking.rooms,
                    guests: booking.guests,
                    agent: booking.agent,
                    status: booking.status,
                    merchant: booking.merchant,
                    total: booking.total
                });
            });
        },

        payBooking: function(id, callback) {
            Booking.lock(id, "BUY_ONLY", "BUY_PENDING", function(err, booking){
                if(!booking) return callback('Booking Not Found');
                var command = {
                    bookingId: booking.id,
                    checkIn: booking.checkIn,
                    checkOut: booking.checkOut,
                    total: booking.total,
                    promoId: booking.rooms[0].promoId,
                    guests: []
                };

                async.waterfall([
                    function(next){
                        async.each(booking.guests,function(guest,callback){
                            Country.findById(guest.nationality,function(err,country){
                                if (err || !country) return callback("nationality");
                                command.guests.push({
                                    firstName:guest.firstName,
                                    lastName:guest.lastName,
                                    email:guest.email,
                                    mobile:guest.mobile,
                                    nationalId:country.meta
                                });
                                return callback();
                            })
                        },function(err){
                            return next(err);
                        })
                    },
                    function(next){
                        command.quantity = 0;
                        command.extrabeds = 0;
                        _(booking.rooms).forEach(function(room){
                            command.quantity = command.quantity + room.quantity;
                            command.extrabeds = command.extrabeds + room.extrabeds;
                        })
                        return next();
                    },
                    function(next){
                        Hotel.findById(booking.hotelId,function(err,hotel){
                            if (err || !hotel) return next('Hotel not found');
                            command.hotelMeta = hotel.meta;
                            _(hotel.rooms).each(function(room){
                                if (room.id === booking.rooms[0].roomId){
                                    command.roomId = room.meta;
                                    command.occupancy = booking.rooms[0].occupancy;
                                    return false;
                                }
                            });
                            City.findById(hotel.city_id,function(err,city){
                                if (err || !city) return next('Hotel not found');
                                command.cityName = city.name.en;
                                return next();
                            })
                        })
                    }
                ],function(err,hotel){
                    if (err){
                        console.log('Create PayCommand Error: ' + err);
                        booking.status.code = 'BUY_ERROR';
                        booking.status.description = err.toString();
                        booking.status.updated = new Date();
                        booking.save();
                    }else{
                        eventBus.emit('hotel.pay.request',{command:command});

                        var handler = function(payload){
                            var result = payload.result;
                            console.log('PayResult ' + JSON.stringify(result));
                            booking.status.code = result.code? result.code: "BUY_UNKNOWN";
                            booking.status.reservationCode = result.reservationCode;
                            booking.status.description = result.msg;
                            booking.status.updated = new Date();
                            booking.save();
                        }

                        executionMap[command.bookingId] = handler;
                        _.delay(function(){
                            if (executionMap[id]){
                                console.log("Timeout delete " + id);
                                var payload = {error:"Time out",result: {msg:"Time out"}}
                                executionMap[id](payload);
                                delete executionMap[id];
                            }
                        },150000)
                    }
                    callback();

                })
            });
        }, 
	}

	register(null,{
		hotelService:hotelService
	})
}