var request = require('request');
var async = require('async');
var _ = require('lodash');
var cheerio = require('cheerio');
var qs = require('querystring');
var AGODA_URL = 'http://xml.agoda.com/datafeeds/Feed.asmx/GetFeed';

var requestQueue = async.queue(function(task,callback){
	console.log("Handler " +task.url);
	request(task.url,function(error,response,body){
		console.log("Completed " + task.url);
		if (!error && response.statusCode == 200){
			callback(null,body);
		}else{
			callback("Request failed");
		}
	})
}, 3);

function getHotelsInCity (city,callback){
		async.waterfall([
			function(next){
				var queryArgs = {
					feed_id: "5",
					apikey : "e3f60702-ae3b-4b96-b41d-1baba49ad734",
					oCity_id: city.meta.agoda_id|| 'none',
					olanguage_id : "24"//"24"
				}
				queryArgs = qs.stringify(queryArgs);
				var url = AGODA_URL + "?" + queryArgs;
				requestQueue.push({url:url},function(err,body){
					if (err) return next(err);
					parseAgodaHotelsForIdNameStarLocation(body,function(hotels){
						console.log(hotels);
						return next(null,hotels);
					})
				});	
			},
			function(hotels,next){
				var hotelResult = []
				async.eachSeries(hotels,function(hotel,doneCallback){
					parseHotelInfo(hotel,function(err,hotelResult){
						console.log(JSON.stringify(hotelResult));
						_.merge(hotel,hotelResult);
						hotelResult.push(hotel);
						doneCallback();
					})
				},function(err){
					if (err) return next(err);

					return next(null,hotelResult);
				})
			}
		],function(err,hotelResult){
			if (err) return callback(err);
			return callback(null,hotelResult);

		})
}

function parseAgodaHotelsForIdNameStarLocation (xmlBody,callback){
	var $ = cheerio.load(xmlBody,{ignoreWhitespace: true, xmlMode: true,lowerCaseTags:true});
	var hotels = [];
	
	$('hotel').each(function(){
		var hotel = {};
		hotel.meta = {};
		hotel.name = {};
		hotel.location = {};
		hotel.meta.agoda_id = $(this).find('hotel_id').text();
		hotel.meta.rating = $(this).find('rating_average').text();
		hotel.rates_from = $(this).find('rates_from').text();
		hotel.name.en = $(this).find('hotel_name').text();
		hotel.name.vi = $(this).find('translated_name').text();
		hotel.star = $(this).find('star_rating').text();
		hotel.location.longitude = $(this).find('longitude').text();
		hotel.location.latitude = $(this).find('latitude').text();
		hotels.push (hotel);
	});
	callback(hotels);
}


function parseHotelInfo(hotel,callback){
	async.waterfall([
		function(next){
			var queryArgs = {
				feed_id: "19",
				apikey :"e3f60702-ae3b-4b96-b41d-1baba49ad734",
				olanguage_id: "24",
				mhotel_id: hotel.meta.agoda_id
			}
			queryArgs = qs.stringify(queryArgs);
			var url = AGODA_URL + '?' + queryArgs;
			requestQueue.push({url:url},function(err,body){
				if (err) return next(err);
				parseHotelInfoVN(body,function(hotelResult){
					//console.log(JSON.stringify(hotelResult));
					_.merge(hotel,hotelResult);
					//console.log(JSON.stringify(hotel));
					return next(null,hotel);
				})
			})
		},
		function(hotel,next){
			var queryArgs = {
				feed_id : "19",
				apikey :"e3f60702-ae3b-4b96-b41d-1baba49ad734",
				mhotel_id: hotel.meta.agoda_id
			}
			queryArgs = qs.stringify(queryArgs);
			var url = AGODA_URL + '?' + queryArgs;
			requestQueue.push({url:url},function(err,body){
				if (err) return next(err);
				parseHotelInfoEN(body,function(hotelResult){
					_.merge(hotel,hotelResult);
					return next(null,hotel);
				})
			})
		}
	],function(err,hotel){
		if (err) return callback(err);
		return callback(null,hotel);
	})
}

function parseHotelInfoVN (xmlBody,callback){
	var $ = cheerio.load(xmlBody);
	var hotel = {};
	hotel.pictures = [];
	$('picture').each(function(){
		hotel.pictures.push($(this).find('URL').text());
	})

	hotel.facilities = {};
	hotel.facilities.hotel = {};
	hotel.facilities.hotel.en = [];
	hotel.facilities.hotel.vi = [];

	$('facility').each(function(){
		hotel.facilities.hotel.en.push($(this).find('property_name').text());
		hotel.facilities.hotel.vi.push($(this).find('property_translated_name').text());
	});

	hotel.overview = {};
	hotel.overview.vi = $('overview').text();

	hotel.rooms = [];
	$('roomtype').each(function(){
		var room = {};
		room.meta = {};
		room.name ={};
		room.max_extrabeds = $(this).find('max_extrabeds').text();
		room.max_occupancy = $(this).find('max_occupancy_per_room').text();
		room.meta.agoda_id = $(this).find('hotel_room_type_id').text();
		room.name.en=$(this).find('standard_caption').text();
		room.name.vi = $(this).find('standard_caption_translated').text();
		room.picture = $(this).find('hotel_room_type_picture').text();
		hotel.rooms.push(room);
	})
	return callback(hotel);
}

function parseHotelInfoEN (xmlBody,callback){
	var $ = cheerio.load(xmlBody);
	var hotel = {};
	hotel.overview = {};
	hotel.overview.en = $('overview').text();
	return callback(hotel);
}

var city = {
	meta:{
		agoda_id:"17160"
	}
}

getHotelsInCity(city,function(err,hotelResult){
	console.log(JSON.stringify(hotelResult));
})