var _ = require('lodash');
var command = {
	agent:{
		id : "abcdef",
		mobile : "0933093320",
		email : "hai.hoang@mobivi.com"
	},
	guests :[
		{
			firstName : "Hai",
			lastName : "Hoang",
			email :"hai.hoang@mobivi.com",
			mobile : "0933093320",
		}
	],
	book:{
		hotelRateId:"535c702aefas4156fa",
		rooms:[
			{
				roomRateId: "3",
				quantity: 1
			}
		]
	}
}



{
  "agent":{
        "id" : "abcdef",
        "mobile" : "0933093320",
        "email" : "hai.hoang@mobivi.com"
  },
    "guests" :[
        {
          "firstName":"Hai",
          "lastName":"Hoang",
          "email":"hai.hoang@mobivi.com",
          "mobile":"0933093320",
          "nationality":"VN"
        }
    ],
    "book":{
        "hotelRateId":"535c702aefas4156fa",
        "rooms":[
            {
                "roomRateId": "3",
                "quantity": 1
            }
        ]
    }
}



var hotelRate = 
    {
    "hotelRateId": "5376c58e914e653603eccb6e",
    "hotelId": "5375796790a2e5d002b19865",
    "checkIn": "2014-07-01",
    "checkOut": "2014-07-05",
    "paxSettings": {
        "submit": "one",
        "childage": "11"
    },
    "rates": [{
        "roomId": "5375796790a2e5d002b1986e",
        "remainingRooms": 5,
        "ratePlanId": "127903",
        "bookingTarget": "W",
        "rate": {
            "daily": [{
                "date": "2014-07-01",
                "exclusive": 1135945,
                "tax": 121659,
                "fee": 0,
                "inclusive": 1257604
            }, {
                "date": "2014-07-02",
                "exclusive": 1135945,
                "tax": 121659,
                "fee": 0,
                "inclusive": 1257604
            }, {
                "date": "2014-07-03",
                "exclusive": 1135945,
                "tax": 121659,
                "fee": 0,
                "inclusive": 1257604
            }, {
                "date": "2014-07-04",
                "exclusive": 1135945,
                "tax": 121659,
                "fee": 0,
                "inclusive": 1257604
            }, {
                "date": "2014-07-05",
                "exclusive": 1135945,
                "tax": 121659,
                "fee": 0,
                "inclusive": 1257604
            }],
            "total": 6288020
        },
        "included": "*Inclusive International Buffet Breakfast, Free WIFI in all room, Free airport transfer round trip and 30 minute foot massage.",
        "cancellation": {
            "summary": {
                "en": "Cancellations or changes to bookings for this room must be made at least 7 days prior to the check in date and time or you will be charged the full amount of the booking.",
                "vi": "Khách hàng sẽ bị phạt toàn bộ số tiền đặt phòng nếu thông báo hủy hoặc thay đổi trong vòng 7 ngày trước giờ nhận phòng quy định của khách sạn"
            }
        },
        "occupancy": 2,
        "extrabeds": 0,
        "roomRateId": "0"
    }, {
        "roomId": "5375796790a2e5d002b1986e",
        "remainingRooms": 5,
        "ratePlanId": "127903",
        "bookingTarget": "W",
        "rate": {
            "daily": [{
                "date": "2014-07-01",
                "exclusive": 1135945,
                "tax": 121660,
                "fee": 628802,
                "inclusive": 1886407
            }, {
                "date": "2014-07-02",
                "exclusive": 1135945,
                "tax": 121660,
                "fee": 628802,
                "inclusive": 1886407
            }, {
                "date": "2014-07-03",
                "exclusive": 1135945,
                "tax": 121660,
                "fee": 628802,
                "inclusive": 1886407
            }, {
                "date": "2014-07-04",
                "exclusive": 1135945,
                "tax": 121660,
                "fee": 628802,
                "inclusive": 1886407
            }, {
                "date": "2014-07-05",
                "exclusive": 1135945,
                "tax": 121660,
                "fee": 628802,
                "inclusive": 1886407
            }],
            "total": 9432035
        },
        "included": "*Inclusive International Buffet Breakfast, Free WIFI in all room, Free airport transfer round trip and 30 minute foot massage.",
        "cancellation": {
            "summary": {
                "en": "Cancellations or changes to bookings for this room must be made at least 7 days prior to the check in date and time or you will be charged the full amount of the booking.",
                "vi": "Khách hàng sẽ bị phạt toàn bộ số tiền đặt phòng nếu thông báo hủy hoặc thay đổi trong vòng 7 ngày trước giờ nhận phòng quy định của khách sạn"
            }
        },
        "occupancy": 2,
        "extrabeds": 1,
        "roomRateId": "1"
    }, {
        "roomId": "5375796790a2e5d002b1986d",
        "remainingRooms": 5,
        "ratePlanId": "127906",
        "bookingTarget": "W",
        "rate": {
            "daily": [{
                "date": "2014-07-01",
                "exclusive": 1590323,
                "tax": 170323,
                "fee": 0,
                "inclusive": 1760646
            }, {
                "date": "2014-07-02",
                "exclusive": 1590323,
                "tax": 170323,
                "fee": 0,
                "inclusive": 1760646
            }, {
                "date": "2014-07-03",
                "exclusive": 1590323,
                "tax": 170323,
                "fee": 0,
                "inclusive": 1760646
            }, {
                "date": "2014-07-04",
                "exclusive": 1590323,
                "tax": 170323,
                "fee": 0,
                "inclusive": 1760646
            }, {
                "date": "2014-07-05",
                "exclusive": 1590323,
                "tax": 170323,
                "fee": 0,
                "inclusive": 1760646
            }],
            "total": 8803230
        },
        "included": "*Inclusive International Buffet Breakfast, Free WIFI in all room, Free airport transfer round trip and 30 minute foot massage.",
        "cancellation": {
            "summary": {
                "en": "Cancellations or changes to bookings for this room must be made at least 7 days prior to the check in date and time or you will be charged the full amount of the booking.",
                "vi": "Khách hàng sẽ bị phạt toàn bộ số tiền đặt phòng nếu thông báo hủy hoặc thay đổi trong vòng 7 ngày trước giờ nhận phòng quy định của khách sạn"
            }
        },
        "occupancy": 2,
        "extrabeds": 0,
        "roomRateId": "2"
    }, {
        "roomId": "5375796790a2e5d002b1986d",
        "remainingRooms": 5,
        "ratePlanId": "127906",
        "bookingTarget": "W",
        "rate": {
            "daily": [{
                "date": "2014-07-01",
                "exclusive": 1590323,
                "tax": 170324,
                "fee": 628802,
                "inclusive": 2389449
            }, {
                "date": "2014-07-02",
                "exclusive": 1590323,
                "tax": 170324,
                "fee": 628802,
                "inclusive": 2389449
            }, {
                "date": "2014-07-03",
                "exclusive": 1590323,
                "tax": 170324,
                "fee": 628802,
                "inclusive": 2389449
            }, {
                "date": "2014-07-04",
                "exclusive": 1590323,
                "tax": 170324,
                "fee": 628802,
                "inclusive": 2389449
            }, {
                "date": "2014-07-05",
                "exclusive": 1590323,
                "tax": 170324,
                "fee": 628802,
                "inclusive": 2389449
            }],
            "total": 11947245
        },
        "included": "*Inclusive International Buffet Breakfast, Free WIFI in all room, Free airport transfer round trip and 30 minute foot massage.",
        "cancellation": {
            "summary": {
                "en": "Cancellations or changes to bookings for this room must be made at least 7 days prior to the check in date and time or you will be charged the full amount of the booking.",
                "vi": "Khách hàng sẽ bị phạt toàn bộ số tiền đặt phòng nếu thông báo hủy hoặc thay đổi trong vòng 7 ngày trước giờ nhận phòng quy định của khách sạn"
            }
        },
        "occupancy": 2,
        "extrabeds": 1,
        "roomRateId": "3"
    }, {
        "roomId": "5375796790a2e5d002b1986c",
        "remainingRooms": 5,
        "ratePlanId": "1286119",
        "bookingTarget": "W",
        "rate": {
            "daily": [{
                "date": "2014-07-01",
                "exclusive": 1817512,
                "tax": 194655,
                "fee": 0,
                "inclusive": 2012167
            }, {
                "date": "2014-07-02",
                "exclusive": 1817512,
                "tax": 194655,
                "fee": 0,
                "inclusive": 2012167
            }, {
                "date": "2014-07-03",
                "exclusive": 1817512,
                "tax": 194655,
                "fee": 0,
                "inclusive": 2012167
            }, {
                "date": "2014-07-04",
                "exclusive": 1817512,
                "tax": 194655,
                "fee": 0,
                "inclusive": 2012167
            }, {
                "date": "2014-07-05",
                "exclusive": 1817512,
                "tax": 194655,
                "fee": 0,
                "inclusive": 2012167
            }],
            "total": 10060835
        },
        "included": "*Inclusive International Buffet Breakfast, Free WIFI in all room, Free airport transfer round trip and 30 minute foot massage.",
        "cancellation": {
            "summary": {
                "en": "Cancellations or changes to bookings for this room must be made at least 7 days prior to the check in date and time or you will be charged the full amount of the booking.",
                "vi": "Khách hàng sẽ bị phạt toàn bộ số tiền đặt phòng nếu thông báo hủy hoặc thay đổi trong vòng 7 ngày trước giờ nhận phòng quy định của khách sạn"
            }
        },
        "occupancy": 2,
        "extrabeds": 0,
        "roomRateId": "4"
    }, {
        "roomId": "5375796790a2e5d002b1986c",
        "remainingRooms": 5,
        "ratePlanId": "1286119",
        "bookingTarget": "W",
        "rate": {
            "daily": [{
                "date": "2014-07-01",
                "exclusive": 1817512,
                "tax": 194656,
                "fee": 628802,
                "inclusive": 2640970
            }, {
                "date": "2014-07-02",
                "exclusive": 1817512,
                "tax": 194656,
                "fee": 628802,
                "inclusive": 2640970
            }, {
                "date": "2014-07-03",
                "exclusive": 1817512,
                "tax": 194656,
                "fee": 628802,
                "inclusive": 2640970
            }, {
                "date": "2014-07-04",
                "exclusive": 1817512,
                "tax": 194656,
                "fee": 628802,
                "inclusive": 2640970
            }, {
                "date": "2014-07-05",
                "exclusive": 1817512,
                "tax": 194656,
                "fee": 628802,
                "inclusive": 2640970
            }],
            "total": 13204850
        },
        "included": "*Inclusive International Buffet Breakfast, Free WIFI in all room, Free airport transfer round trip and 30 minute foot massage.",
        "cancellation": {
            "summary": {
                "en": "Cancellations or changes to bookings for this room must be made at least 7 days prior to the check in date and time or you will be charged the full amount of the booking.",
                "vi": "Khách hàng sẽ bị phạt toàn bộ số tiền đặt phòng nếu thông báo hủy hoặc thay đổi trong vòng 7 ngày trước giờ nhận phòng quy định của khách sạn"
            }
        },
        "occupancy": 2,
        "extrabeds": 1,
        "roomRateId": "5"
    }, {
        "roomId": "5375796790a2e5d002b1986b",
        "remainingRooms": 5,
        "ratePlanId": "1286120",
        "bookingTarget": "W",
        "rate": {
            "daily": [{
                "date": "2014-07-01",
                "exclusive": 1741782,
                "tax": 186545,
                "fee": 0,
                "inclusive": 1928327
            }, {
                "date": "2014-07-02",
                "exclusive": 1741782,
                "tax": 186545,
                "fee": 0,
                "inclusive": 1928327
            }, {
                "date": "2014-07-03",
                "exclusive": 1741782,
                "tax": 186545,
                "fee": 0,
                "inclusive": 1928327
            }, {
                "date": "2014-07-04",
                "exclusive": 1741782,
                "tax": 186545,
                "fee": 0,
                "inclusive": 1928327
            }, {
                "date": "2014-07-05",
                "exclusive": 1741782,
                "tax": 186545,
                "fee": 0,
                "inclusive": 1928327
            }],
            "total": 9641635
        },
        "included": "*Inclusive International Buffet Breakfast, Free WIFI in all room, Free airport transfer round trip and 30 minute foot massage.",
        "cancellation": {
            "summary": {
                "en": "Cancellations or changes to bookings for this room must be made at least 7 days prior to the check in date and time or you will be charged the full amount of the booking.",
                "vi": "Khách hàng sẽ bị phạt toàn bộ số tiền đặt phòng nếu thông báo hủy hoặc thay đổi trong vòng 7 ngày trước giờ nhận phòng quy định của khách sạn"
            }
        },
        "occupancy": 2,
        "extrabeds": 0,
        "roomRateId": "6"
    }, {
        "roomId": "5375796790a2e5d002b1986b",
        "remainingRooms": 5,
        "ratePlanId": "1286120",
        "bookingTarget": "W",
        "rate": {
            "daily": [{
                "date": "2014-07-01",
                "exclusive": 1741782,
                "tax": 186545,
                "fee": 628802,
                "inclusive": 2557129
            }, {
                "date": "2014-07-02",
                "exclusive": 1741782,
                "tax": 186545,
                "fee": 628802,
                "inclusive": 2557129
            }, {
                "date": "2014-07-03",
                "exclusive": 1741782,
                "tax": 186545,
                "fee": 628802,
                "inclusive": 2557129
            }, {
                "date": "2014-07-04",
                "exclusive": 1741782,
                "tax": 186545,
                "fee": 628802,
                "inclusive": 2557129
            }, {
                "date": "2014-07-05",
                "exclusive": 1741782,
                "tax": 186545,
                "fee": 628802,
                "inclusive": 2557129
            }],
            "total": 12785645
        },
        "included": "*Inclusive International Buffet Breakfast, Free WIFI in all room, Free airport transfer round trip and 30 minute foot massage.",
        "cancellation": {
            "summary": {
                "en": "Cancellations or changes to bookings for this room must be made at least 7 days prior to the check in date and time or you will be charged the full amount of the booking.",
                "vi": "Khách hàng sẽ bị phạt toàn bộ số tiền đặt phòng nếu thông báo hủy hoặc thay đổi trong vòng 7 ngày trước giờ nhận phòng quy định của khách sạn"
            }
        },
        "occupancy": 2,
        "extrabeds": 1,
        "roomRateId": "7"
    }, {
        "roomId": "5375796790a2e5d002b1986a",
        "remainingRooms": 5,
        "ratePlanId": "1286121",
        "bookingTarget": "W",
        "rate": {
            "daily": [{
                "date": "2014-07-01",
                "exclusive": 1666052,
                "tax": 178435,
                "fee": 0,
                "inclusive": 1844487
            }, {
                "date": "2014-07-02",
                "exclusive": 1666052,
                "tax": 178435,
                "fee": 0,
                "inclusive": 1844487
            }, {
                "date": "2014-07-03",
                "exclusive": 1666052,
                "tax": 178435,
                "fee": 0,
                "inclusive": 1844487
            }, {
                "date": "2014-07-04",
                "exclusive": 1666052,
                "tax": 178435,
                "fee": 0,
                "inclusive": 1844487
            }, {
                "date": "2014-07-05",
                "exclusive": 1666052,
                "tax": 178435,
                "fee": 0,
                "inclusive": 1844487
            }],
            "total": 9222435
        },
        "included": "*Inclusive International Buffet Breakfast, Free WIFI in all room, Free airport transfer round trip and 30 minute foot massage.",
        "cancellation": {
            "summary": {
                "en": "Cancellations or changes to bookings for this room must be made at least 7 days prior to the check in date and time or you will be charged the full amount of the booking.",
                "vi": "Khách hàng sẽ bị phạt toàn bộ số tiền đặt phòng nếu thông báo hủy hoặc thay đổi trong vòng 7 ngày trước giờ nhận phòng quy định của khách sạn"
            }
        },
        "occupancy": 2,
        "extrabeds": 0,
        "roomRateId": "8"
    }, {
        "roomId": "5375796790a2e5d002b1986a",
        "remainingRooms": 5,
        "ratePlanId": "1286121",
        "bookingTarget": "W",
        "rate": {
            "daily": [{
                "date": "2014-07-01",
                "exclusive": 1666053,
                "tax": 178434,
                "fee": 628802,
                "inclusive": 2473289
            }, {
                "date": "2014-07-02",
                "exclusive": 1666053,
                "tax": 178434,
                "fee": 628802,
                "inclusive": 2473289
            }, {
                "date": "2014-07-03",
                "exclusive": 1666053,
                "tax": 178434,
                "fee": 628802,
                "inclusive": 2473289
            }, {
                "date": "2014-07-04",
                "exclusive": 1666053,
                "tax": 178434,
                "fee": 628802,
                "inclusive": 2473289
            }, {
                "date": "2014-07-05",
                "exclusive": 1666053,
                "tax": 178434,
                "fee": 628802,
                "inclusive": 2473289
            }],
            "total": 12366445
        },
        "included": "*Inclusive International Buffet Breakfast, Free WIFI in all room, Free airport transfer round trip and 30 minute foot massage.",
        "cancellation": {
            "summary": {
                "en": "Cancellations or changes to bookings for this room must be made at least 7 days prior to the check in date and time or you will be charged the full amount of the booking.",
                "vi": "Khách hàng sẽ bị phạt toàn bộ số tiền đặt phòng nếu thông báo hủy hoặc thay đổi trong vòng 7 ngày trước giờ nhận phòng quy định của khách sạn"
            }
        },
        "occupancy": 2,
        "extrabeds": 1,
        "roomRateId": "9"
    }, {
        "roomId": "5375796790a2e5d002b19869",
        "remainingRooms": 5,
        "ratePlanId": "201860",
        "bookingTarget": "W",
        "rate": {
            "daily": [{
                "date": "2014-07-01",
                "exclusive": 1457796,
                "tax": 156130,
                "fee": 0,
                "inclusive": 1613926
            }, {
                "date": "2014-07-02",
                "exclusive": 1457796,
                "tax": 156130,
                "fee": 0,
                "inclusive": 1613926
            }, {
                "date": "2014-07-03",
                "exclusive": 1457796,
                "tax": 156130,
                "fee": 0,
                "inclusive": 1613926
            }, {
                "date": "2014-07-04",
                "exclusive": 1457796,
                "tax": 156130,
                "fee": 0,
                "inclusive": 1613926
            }, {
                "date": "2014-07-05",
                "exclusive": 1457796,
                "tax": 156130,
                "fee": 0,
                "inclusive": 1613926
            }],
            "total": 8069630
        },
        "included": "*Inclusive International Buffet Breakfast, Free WIFI in all room, Free airport transfer round trip and 30 minute foot massage.",
        "cancellation": {
            "summary": {
                "en": "Cancellations or changes to bookings for this room must be made at least 7 days prior to the check in date and time or you will be charged the full amount of the booking.",
                "vi": "Khách hàng sẽ bị phạt toàn bộ số tiền đặt phòng nếu thông báo hủy hoặc thay đổi trong vòng 7 ngày trước giờ nhận phòng quy định của khách sạn"
            }
        },
        "occupancy": 2,
        "extrabeds": 0,
        "roomRateId": "10"
    }, {
        "roomId": "5375796790a2e5d002b19869",
        "remainingRooms": 5,
        "ratePlanId": "201860",
        "bookingTarget": "W",
        "rate": {
            "daily": [{
                "date": "2014-07-01",
                "exclusive": 1457796,
                "tax": 156130,
                "fee": 628802,
                "inclusive": 2242728
            }, {
                "date": "2014-07-02",
                "exclusive": 1457796,
                "tax": 156130,
                "fee": 628802,
                "inclusive": 2242728
            }, {
                "date": "2014-07-03",
                "exclusive": 1457796,
                "tax": 156130,
                "fee": 628802,
                "inclusive": 2242728
            }, {
                "date": "2014-07-04",
                "exclusive": 1457796,
                "tax": 156130,
                "fee": 628802,
                "inclusive": 2242728
            }, {
                "date": "2014-07-05",
                "exclusive": 1457796,
                "tax": 156130,
                "fee": 628802,
                "inclusive": 2242728
            }],
            "total": 11213640
        },
        "included": "*Inclusive International Buffet Breakfast, Free WIFI in all room, Free airport transfer round trip and 30 minute foot massage.",
        "cancellation": {
            "summary": {
                "en": "Cancellations or changes to bookings for this room must be made at least 7 days prior to the check in date and time or you will be charged the full amount of the booking.",
                "vi": "Khách hàng sẽ bị phạt toàn bộ số tiền đặt phòng nếu thông báo hủy hoặc thay đổi trong vòng 7 ngày trước giờ nhận phòng quy định của khách sạn"
            }
        },
        "occupancy": 2,
        "extrabeds": 1,
        "roomRateId": "11"
    }, {
        "roomId": "5375796790a2e5d002b19868",
        "remainingRooms": 5,
        "ratePlanId": "201863",
        "bookingTarget": "W",
        "rate": {
            "daily": [{
                "date": "2014-07-01",
                "exclusive": 1022350,
                "tax": 109494,
                "fee": 0,
                "inclusive": 1131844
            }, {
                "date": "2014-07-02",
                "exclusive": 1022350,
                "tax": 109494,
                "fee": 0,
                "inclusive": 1131844
            }, {
                "date": "2014-07-03",
                "exclusive": 1022350,
                "tax": 109494,
                "fee": 0,
                "inclusive": 1131844
            }, {
                "date": "2014-07-04",
                "exclusive": 1022350,
                "tax": 109494,
                "fee": 0,
                "inclusive": 1131844
            }, {
                "date": "2014-07-05",
                "exclusive": 1022350,
                "tax": 109494,
                "fee": 0,
                "inclusive": 1131844
            }],
            "total": 5659220
        },
        "included": "*Inclusive International Buffet Breakfast, Free WIFI in all room and 30 minute foot massage.",
        "cancellation": {
            "summary": {
                "en": "Cancellations or changes to bookings for this room must be made at least 7 days prior to the check in date and time or you will be charged the full amount of the booking.",
                "vi": "Khách hàng sẽ bị phạt toàn bộ số tiền đặt phòng nếu thông báo hủy hoặc thay đổi trong vòng 7 ngày trước giờ nhận phòng quy định của khách sạn"
            }
        },
        "occupancy": 2,
        "extrabeds": 0,
        "roomRateId": "12"
    }, {
        "roomId": "5375796790a2e5d002b19868",
        "remainingRooms": 5,
        "ratePlanId": "201863",
        "bookingTarget": "W",
        "rate": {
            "daily": [{
                "date": "2014-07-01",
                "exclusive": 1022351,
                "tax": 109493,
                "fee": 628802,
                "inclusive": 1760646
            }, {
                "date": "2014-07-02",
                "exclusive": 1022351,
                "tax": 109493,
                "fee": 628802,
                "inclusive": 1760646
            }, {
                "date": "2014-07-03",
                "exclusive": 1022351,
                "tax": 109493,
                "fee": 628802,
                "inclusive": 1760646
            }, {
                "date": "2014-07-04",
                "exclusive": 1022351,
                "tax": 109493,
                "fee": 628802,
                "inclusive": 1760646
            }, {
                "date": "2014-07-05",
                "exclusive": 1022351,
                "tax": 109493,
                "fee": 628802,
                "inclusive": 1760646
            }],
            "total": 8803230
        },
        "included": "*Inclusive International Buffet Breakfast, Free WIFI in all room and 30 minute foot massage.",
        "cancellation": {
            "summary": {
                "en": "Cancellations or changes to bookings for this room must be made at least 7 days prior to the check in date and time or you will be charged the full amount of the booking.",
                "vi": "Khách hàng sẽ bị phạt toàn bộ số tiền đặt phòng nếu thông báo hủy hoặc thay đổi trong vòng 7 ngày trước giờ nhận phòng quy định của khách sạn"
            }
        },
        "occupancy": 2,
        "extrabeds": 1,
        "roomRateId": "13"
    }, {
        "roomId": "5375796790a2e5d002b19867",
        "remainingRooms": 5,
        "ratePlanId": "227891",
        "bookingTarget": "W",
        "rate": {
            "daily": [{
                "date": "2014-07-01",
                "exclusive": 1079147,
                "tax": 115577,
                "fee": 0,
                "inclusive": 1194724
            }, {
                "date": "2014-07-02",
                "exclusive": 1079147,
                "tax": 115577,
                "fee": 0,
                "inclusive": 1194724
            }, {
                "date": "2014-07-03",
                "exclusive": 1079147,
                "tax": 115577,
                "fee": 0,
                "inclusive": 1194724
            }, {
                "date": "2014-07-04",
                "exclusive": 1079147,
                "tax": 115577,
                "fee": 0,
                "inclusive": 1194724
            }, {
                "date": "2014-07-05",
                "exclusive": 1079147,
                "tax": 115577,
                "fee": 0,
                "inclusive": 1194724
            }],
            "total": 5973620
        },
        "included": "*Inclusive International Buffet Breakfast, Free WIFI in all room ,30 minute foot massage and round trip transfer provided.",
        "cancellation": {
            "summary": {
                "en": "Cancellations or changes to bookings for this room must be made at least 7 days prior to the check in date and time or you will be charged the full amount of the booking.",
                "vi": "Khách hàng sẽ bị phạt toàn bộ số tiền đặt phòng nếu thông báo hủy hoặc thay đổi trong vòng 7 ngày trước giờ nhận phòng quy định của khách sạn"
            }
        },
        "occupancy": 2,
        "extrabeds": 0,
        "roomRateId": "14"
    }, {
        "roomId": "5375796790a2e5d002b19867",
        "remainingRooms": 5,
        "ratePlanId": "227891",
        "bookingTarget": "W",
        "rate": {
            "daily": [{
                "date": "2014-07-01",
                "exclusive": 1079148,
                "tax": 115576,
                "fee": 628802,
                "inclusive": 1823526
            }, {
                "date": "2014-07-02",
                "exclusive": 1079148,
                "tax": 115576,
                "fee": 628802,
                "inclusive": 1823526
            }, {
                "date": "2014-07-03",
                "exclusive": 1079148,
                "tax": 115576,
                "fee": 628802,
                "inclusive": 1823526
            }, {
                "date": "2014-07-04",
                "exclusive": 1079148,
                "tax": 115576,
                "fee": 628802,
                "inclusive": 1823526
            }, {
                "date": "2014-07-05",
                "exclusive": 1079148,
                "tax": 115576,
                "fee": 628802,
                "inclusive": 1823526
            }],
            "total": 9117630
        },
        "included": "*Inclusive International Buffet Breakfast, Free WIFI in all room ,30 minute foot massage and round trip transfer provided.",
        "cancellation": {
            "summary": {
                "en": "Cancellations or changes to bookings for this room must be made at least 7 days prior to the check in date and time or you will be charged the full amount of the booking.",
                "vi": "Khách hàng sẽ bị phạt toàn bộ số tiền đặt phòng nếu thông báo hủy hoặc thay đổi trong vòng 7 ngày trước giờ nhận phòng quy định của khách sạn"
            }
        },
        "occupancy": 2,
        "extrabeds": 1,
        "roomRateId": "15"
    }, {
        "roomId": "5375796790a2e5d002b19866",
        "remainingRooms": 5,
        "ratePlanId": "227892",
        "bookingTarget": "W",
        "rate": {
            "daily": [{
                "date": "2014-07-01",
                "exclusive": 1514593,
                "tax": 162213,
                "fee": 0,
                "inclusive": 1676806
            }, {
                "date": "2014-07-02",
                "exclusive": 1514593,
                "tax": 162213,
                "fee": 0,
                "inclusive": 1676806
            }, {
                "date": "2014-07-03",
                "exclusive": 1514593,
                "tax": 162213,
                "fee": 0,
                "inclusive": 1676806
            }, {
                "date": "2014-07-04",
                "exclusive": 1514593,
                "tax": 162213,
                "fee": 0,
                "inclusive": 1676806
            }, {
                "date": "2014-07-05",
                "exclusive": 1514593,
                "tax": 162213,
                "fee": 0,
                "inclusive": 1676806
            }],
            "total": 8384030
        },
        "included": "*Inclusive International Buffet Breakfast, Free WIFI in all room, Free airport transfer round trip and 30 minute foot massage.",
        "cancellation": {
            "summary": {
                "en": "Cancellations or changes to bookings for this room must be made at least 7 days prior to the check in date and time or you will be charged the full amount of the booking.",
                "vi": "Khách hàng sẽ bị phạt toàn bộ số tiền đặt phòng nếu thông báo hủy hoặc thay đổi trong vòng 7 ngày trước giờ nhận phòng quy định của khách sạn"
            }
        },
        "occupancy": 2,
        "extrabeds": 0,
        "roomRateId": "16"
    }, {
        "roomId": "5375796790a2e5d002b19866",
        "remainingRooms": 5,
        "ratePlanId": "227892",
        "bookingTarget": "W",
        "rate": {
            "daily": [{
                "date": "2014-07-01",
                "exclusive": 1514593,
                "tax": 162213,
                "fee": 628802,
                "inclusive": 2305608
            }, {
                "date": "2014-07-02",
                "exclusive": 1514593,
                "tax": 162213,
                "fee": 628802,
                "inclusive": 2305608
            }, {
                "date": "2014-07-03",
                "exclusive": 1514593,
                "tax": 162213,
                "fee": 628802,
                "inclusive": 2305608
            }, {
                "date": "2014-07-04",
                "exclusive": 1514593,
                "tax": 162213,
                "fee": 628802,
                "inclusive": 2305608
            }, {
                "date": "2014-07-05",
                "exclusive": 1514593,
                "tax": 162213,
                "fee": 628802,
                "inclusive": 2305608
            }],
            "total": 11528040
        },
        "included": "*Inclusive International Buffet Breakfast, Free WIFI in all room, Free airport transfer round trip and 30 minute foot massage.",
        "cancellation": {
            "summary": {
                "en": "Cancellations or changes to bookings for this room must be made at least 7 days prior to the check in date and time or you will be charged the full amount of the booking.",
                "vi": "Khách hàng sẽ bị phạt toàn bộ số tiền đặt phòng nếu thông báo hủy hoặc thay đổi trong vòng 7 ngày trước giờ nhận phòng quy định của khách sạn"
            }
        },
        "occupancy": 2,
        "extrabeds": 1,
        "roomRateId": "17"
    }]
}
var mongoose = require('mongoose');
var urlString = 'mongodb://hoangnhathai:Aesx5099@ds043329.mongolab.com:43329/ivivu_crawl'
mongoose.connect(urlString,function(err,res){
	if (err){
		console.log("Connect failed " + err );
	}else{
		console.log("Connect success");
	}
})

var Booking = require("../Model/HotelBooking.js");
var Hotel = require("../Model/Hotel.js");

var createBooking = function(command,callback){
	var booking = new Booking();
	booking.hotelId = hotelRate.hotelId;
    booking.checkIn = hotelRate.checkIn;
    booking.checkOut = hotelRate.checkOut;
    booking.agent = command.agent;
    booking.guests = command.guests;        
    booking.rooms = [];
    booking.total = 0;
    _(command.book.rooms).forEach(function(room){
    var tmp = _.find(hotelRate.rates, function(rate) {return rate.roomRateId == room.roomRateId;});
    console.log(JSON.stringify(tmp.ratePlanId));
    console.log(JSON.stringify(tmp.bookingTarget));
    if(!tmp) return false;
    booking.rooms.push({
	        quantity: room.quantity,
	        roomId: tmp.roomId,
	        ratePlanId: tmp.ratePlanId,
	        bookingTarget : tmp.bookingTarget,
	        occupancy: tmp.occupancy,
	        extrabeds: tmp.extrabeds,
	        rate: tmp.rate,
	        promoId: tmp.promoId,
	        included: tmp.included,
	        cancellation: tmp.cancellation											
	    });						
	    booking.total = booking.total + tmp.rate.total * room.quantity;
	});


	if(booking.rooms.length != command.book.rooms.length){
	    return callback('Invalid Room Rate');
	}

	booking.save(function(err){
		callback(null,booking);
	})

}

var payBooking = function(id,callback){
	Booking.lock(id,"BUY_ONLY","BUY_PENDING",function(err,booking){
		//console.log(JSON.stringify(booking));
		if (!booking) return callback("Booking not found");
		var command = {
			quantity : booking.rooms[0].quantity,
			occupancy : booking.rooms[0].occupancy,
			checkIn: booking.checkIn,
			checkOut: booking.checkOut,
			guestInfo:{
				firstName:booking.guests[0].firstName,
				lastName:booking.guests[0].lastName,
				email:booking.guests[0].email,
				mobile:booking.guests[0].mobile,
			},
			ratePlanId: booking.rooms[0].ratePlanId,
			bookingTarget : booking.rooms[0].bookingTarget,
			total:booking.total
		}
		Hotel.findById(booking.hotelId,function(err,hotel){
			if (!hotel) return callback("ERROR, HOTEL NOT FOUND");
			command.hotelId = hotel.meta.ivivu_id;
			var room = _.find(hotel.rooms,function(room){return room.id == booking.rooms[0].roomId});
			if (!room) {
				return callback("NO ROOM MATCHED");
			}else{
				command.roomId = room.meta.ivivu_id;
				return callback(null,command);
			}
		})
	})
}


var xmlObj = {
    "quantity": 1,
    "occupancy": 2,
    "checkIn": "2014-07-01",
    "checkOut": "2014-07-05",
    "guestInfo": {
        "firstName": "Hai",
        "lastName": "Hoang",
        "email": "hai.hoang@mobivi.com",
        "mobile": "0933093320"
    },
    "ratePlanId": "127906",
    "bookingTarget": "W",
    "total": 11984975,
    "hotelId": "W53959",
    "roomId": "127906"
}

//{"quantity":2,"occupancy":2,"checkIn":"2014-07-01","checkOut":"2014-07-05","guestInfo":{"firstName":"Hai","lastName":"Hoang","email":"hai.hoang@mobivi.com","mobile":"0933093320"},"total":23969950,"hotelId":"W53959","roomId":"127906"}



// payBooking("5375efc7944b7ae409f141e4",function(err,command){
// 	console.log(JSON.stringify(command));
// })
createBooking(command,function(err,booking){
	console.log(JSON.stringify(booking));
})

 