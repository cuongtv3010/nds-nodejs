var hotelService = require('../hotel/index.js');
var agoda_availability = require('../AGODA/availability.js');
var ivivu_availability = require('../IVIVU/availability.js');
var _ = require('lodash');


//Agoda :53747013762e1a8108a8eb18
//Ivivu :5372f0107b3cc19e0dc2e90b
//Ivivu :5375796790a2e5d002b19865
var hotelID = "5375796790a2e5d002b19865"
var checkIn = '2014-08-09';
var checkOut = '2014-08-11';

var mongoose = require('mongoose');
var urlString = 'mongodb://hoangnhathai:Aesx5099@ds043329.mongolab.com:43329/ivivu_crawl'
mongoose.connect(urlString,function(err,res){
	if (err){
		console.log("Connect failed " + err );
	}else{
		console.log("Connect success");
	}
})

var Hotel = require("../Model/Hotel.js");

var merchants = [
	{
		name:"AGODA",
		merchant_id: "agoda_id",
		availability : agoda_availability
	},
	{
		name:"IVIVU",
		merchant_id: "ivivu_id",
		availability:ivivu_availability
	}
]

var checkRate = function(hotelId,checkIn,checkOut,callback){
	Hotel.findById(hotelId,function(err,hotel){
		if (err) return callback("Can't find hotel");
		var command = {
			id: new mongoose.Types.ObjectId(),
			type: 'checkAvailability',
			payload:{
				hotelId: hotel.id,
	            meta: {},
	            checkIn: checkIn,
	            checkOut: checkOut,
	            rooms: []
			}
		};
		
		var merchant = _.find(merchants,function(correctMerchant){
			return (hotel.meta[correctMerchant.merchant_id]);
		})
		var merchant_id= merchant.merchant_id;

		if (!hotel.meta[merchant_id]) return callback();
		command.payload.meta[merchant_id] = hotel.meta[merchant_id];

		_(hotel.rooms).forEach(function(room){
			var commandRoom = {
				id:room.id,
				max_occupancy:room.max_occupancy,
				max_extrabeds:room.max_extrabeds,
				meta:{}
			};
			commandRoom.meta[merchant_id] = room.meta[merchant_id];
			command.payload.rooms.push(commandRoom);
		});

		
		merchant.availability.check(command,function(err,result){
			 if (err) return callback(err);
			 return callback(null,result);
		})
	})
}
	

checkRate(hotelID,checkIn,checkOut,function(err,result){
	console.log(JSON.stringify(result));
	console.log("Done");
})